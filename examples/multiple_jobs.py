# This script contains a dummy example of how to run a grid search across many different jobs.

import uproot as up

from dream.utils.io import launch_batch_jobs


def data_loader(data_file, features=None, include_integers=True):
    """
    Function for configuring the DREAM pipeline.
    :param data_file: a string pointing to a specific root file
    :param features: a list (or string separated by commas) of features to train on. In the default loader, passing
    None will take all values.
    :param include_integers: a boolean, set to True to include integer type features.
    :return: A tuple of pandas DataFrame and string for the model name (containing information on energy/eta/...
             slice)
    """
    # Get the name of the model {FCS, AF2/3, Fast Calo GAN, ....}
    model_name = data_file.split('/')[-1]

    # Define the jet algorithm and features to train with
    jet_algorithm = 'AntiKt4EMPFlowJetsAuxDyn'
    varnames = [f'{jet_algorithm}.N90Constituents', f'{jet_algorithm}.AverageLArQF']
    # Load the file
    with up.open(data_file) as f:
        data = f['CollectionTree'].arrays(varnames, library="pd")

    return data, model_name


def geant_4_conditions(file_name):
    """
    Function for identifying geant4 files.
    :param file_name: A string that points to a file that can be read with the already defined data_loader.
    :return:
    """
    return file_name[-6:] == 'GEANT4'


def get_properties(file_name):
    """
    A function for identifying the hierarchies/slices in the data.
    :param file_name: A string that labels a given data file, all directory information will have been stripped.
    :return: The three levels on which to separate the different files, and a name to tag this file with. In
    the fast calo sim case this might be the particle, energy, eta and flavor of simulation.
    """
    level_0, level_1, level_2, name = file_name.split('_')
    return level_0, level_1, level_2, name


if __name__ == '__main__':
    # Define the directories under which the pipeline will search for files
    data_directories = ['/data/GEANT4', '/data/FCS', '/data/extras']

    launch_batch_jobs(data_directories, 'test', 'dummy_example', data_loader=data_loader,
                      conditions=geant_4_conditions, get_properties=get_properties)
