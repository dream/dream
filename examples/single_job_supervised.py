# This script contains a dummy example of how to run a supervised training on a single sample.import numpy as np
import pandas as pd
import uproot as up
import numpy as np

from dream.supervised_classifier import get_supervised_args, supervised_classifier


def physics_example(args):
    """This provides a dummy example of how this can be applied to a physics dataset."""
    def data_loader(data_file, **kwargs):
        # Get the name of the model {FCS, AF2/3, Fast Calo GAN, ....}
        model_name = data_file.split('/')[-1]

        # Define the jet algorithm and features to train with
        jet_algorithm = 'AntiKt4EMPFlowJetsAuxDyn'
        varnames = [f'{jet_algorithm}.N90Constituents', f'{jet_algorithm}.AverageLArQF']
        # Load the file
        with up.open(data_file) as f:
            data = f['CollectionTree'].arrays(varnames, library="pd")

        return data, model_name

    args.outputname = 'physics_demo'
    supervised_classifier(args, data_loader)
    return 0

def data_loader(name, **kwargs):
    # Define the dataset
    n_samples = 20000
    n_features = 8
    model = name.split('/')[-1]
    data = pd.DataFrame(data=np.random.normal(size=(n_samples, n_features)),
                        columns=np.arange(0, n_features),
                        index=np.arange(0, n_samples))
    # In one of the two samples we can introduce some shifts/mismodelling
    if model == 'correlated':
        # Sort the first and second axes, this won't change 1D hist, but do change higher dim correlations
        data.iloc[:, 0] = np.sort(data.iloc[:, 0])
        data.iloc[:, 1] = np.sort(data.iloc[:, 1])
        # Introduce a linear shift to the third component
        data.iloc[:, 2] += 0.1
    return data, model


def pass_data_loader_directly(args):
    """
    In this example the data loader is passed directly to the calling function.
    This is an example that can actually be run, fake data will be generated to demonstrate the use of the pipeline.
    It is simple to change the args if you want to, this is just done like this here for simplicity.
    """
    # Set some of the args by hand
    args.data_files = 'uncorrelated,correlated'
    args.epochs = 100
    args.nfolds = 5
    args.outputname = 'correlations_demo'
    args.load = 0
    # Run the supervised classifier
    supervised_classifier(args, data_loader)

def point_to_this_file(args):
    """
    In this example the location of this example is passed as an arg, this is typically how multiple jobs can be run.
    """
    # Set the path to this file. Note that it has to be in the form of a python import statement. That also means that
    # you need to have an __init__.py in any subdirectories you create.
    args.data_loader_file = 'examples.single_job_supervised'
    args.load = 1
    # Run the supervised classifier
    supervised_classifier(args, data_loader)


if __name__ == '__main__':
    args = get_supervised_args()
    args.outputdir = 'demo'
    pass_data_loader_directly(args)
    point_to_this_file(args)
