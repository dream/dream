#!/bin/bash
SINGULARITY_TOP_DIR=/eos/user/s/saklein
export SINGULARITY_CACHEDIR=$SINGULARITY_TOP_DIR/.singularity
export PYTHONPATH=${PWD}:${PWD}/python_install:${PYTHONPATH}

singularity exec --nv -B ${PWD},/pool,/eos,/afs,$SINGULARITY_TOP_DIR $SINGULARITY_TOP_DIR/DREAM/container/pillow.sif python3 ${PWD}/examples/single_job_supervised.py
