"""Tests for the PyTorch utility functions."""
import os
import unittest

import numpy as np
import pandas as pd

from dream.utils.io import save_info
import torchtestcase
from dream.supervised_classifier import get_supervised_args, supervised_classifier
from sklearn.metrics import roc_auc_score


class SupervisedClassifierTest(torchtestcase.TorchTestCase):

    def setUp(self) -> None:
        args = get_supervised_args()
        self.sample_info = 'second'
        args.data_files = f'first,{self.sample_info}'
        args.outputdir = 'unit_test'
        self.args = args
        self.n_samples = 20000
        self.n_features = 5
        self.svo = save_info(directory=self.args.outputdir, name=self.args.outputname)

    def end_experiment(self):
        os.remove(self.svo.get_image_dir())

    def test_dist_vs_dist(self):
        """Check to see that the classifier doesn't separate a normal distribution from a normal distribution."""
        def data_loader(name, **kwargs):
            model = name.split('/')[-1]
            data = pd.DataFrame(data=np.random.normal(size=(self.n_samples, self.n_features)),
                                columns=np.arange(0, self.n_features),
                                index=np.arange(0, self.n_samples))
            return data, model

        self.args.epochs = 10
        self.args.nfolds = 3
        supervised_classifier(self.args, data_loader)
        with open(self.svo.set_name('eval_info', '.np', sample_info=self.sample_info), 'rb') as file:
            y_true = np.load(file)
            y_pred = np.load(file)
            saliency = np.load(file)
            train_loss = np.load(file)
            valid_loss = np.load(file)

        with open(self.svo.set_name('auc', '.np', sample_info=self.sample_info), 'rb') as file:
            auc = np.load(file)
            saliency_two = np.load(file)

        # Check that the classifier didn't learn to separate the same distribution from itself. Note only training for
        # ten epochs so don't expect this to be exact.
        with self.subTest('Cannot separate distributions.'):
            self.assertAlmostEqual(auc, 0.5, places=2)
        # Check the evaluation script doesn't do anything weird, and saving/loading in these scripts works.
        loaded_auc = roc_auc_score(y_true, y_pred)
        with self.subTest('Loading AUC works.'):
            self.assertEqual(auc, loaded_auc)
        with self.subTest('Loading saliency.'):
            np.testing.assert_array_equal(saliency_two, saliency)
        # Check that the saliency is the same for each feature to a reasonable accuracy with this number of samples and
        # training epochs, more training and more data would result in higher accuracy here.
        salience = np.around(saliency[0], decimals=2)
        with self.subTest('Equivalent saliency.'):
            np.testing.assert_array_almost_equal(saliency, np.array([salience] * self.n_features), decimal=2)

        # self.end_experiment()

    def test_dist_vs_shifted_dist(self):
        """Check to see that the classifier can separate a normal distribution from a shifted normal distribution."""
        def data_loader(name, **kwargs):
            model = name.split('/')[-1]
            data = pd.DataFrame(data=np.random.normal(size=(self.n_samples, self.n_features)),
                                columns=np.arange(0, self.n_features),
                                index=np.arange(0, self.n_samples))
            if model == 'second':
                # Shift the gaussian along every axis by a decreasing amount
                data += np.linspace(3, 0, self.n_features)
            return data, model

        # To get the correct feature ordering this training needs to be run for quite a long time, this feature of the
        # tool is highly dependent on the training time.
        self.args.epochs = 100
        self.args.nfolds = 5
        supervised_classifier(self.args, data_loader)

        # This will load the arrays that are saved at the end of supervised_training.py
        with open(self.svo.set_name('eval_info', '.np', sample_info=self.sample_info), 'rb') as file:
            y_true = np.load(file)
            y_pred = np.load(file)
            saliency = np.load(file)
            train_loss = np.load(file)
            valid_loss = np.load(file)
            shapes = np.load(file)

        cnt = 0
        for i, shape in enumerate(shapes):
            auc = roc_auc_score(y_true[cnt:cnt+shape], y_pred[cnt:cnt+shape])
            cnt += shape
            with self.subTest(f'Testing AUC of fold {i}.'):
                self.assertGreater(auc, 0.5)

        # This will load the arrays that are saved across folds in the dream.utils.evaluate_folds function
        with open(self.svo.set_name('auc', '.np', sample_info=self.sample_info), 'rb') as file:
            auc = np.load(file)
            saliency_two = np.load(file)

        with self.subTest('Testing AUC of combined folds.'):
            self.assertGreater(auc, 0.5)

        with self.subTest('Test feature ordering.'):
            self.assertListEqual(list(np.argsort(saliency_two)), list(range(self.n_features)))

        # self.end_experiment()

    def test_uncorrelated_vs_correlated(self):
        """
        Check to see that the classifier can separate an uncorrelated distribution from a correlated normal
        distribution.
        """

        def data_loader(name, **kwargs):
            model = name.split('/')[-1]
            data = pd.DataFrame(data=np.random.normal(size=(self.n_samples, self.n_features)),
                                columns=np.arange(0, self.n_features),
                                index=np.arange(0, self.n_samples))
            if model == 'second':
                # Sort the first and second axes, don't change 1D hist, but do change higher dim correlations
                data.iloc[:, 0] = np.sort(data.iloc[:, 0])
                data.iloc[:, 1] = np.sort(data.iloc[:, 1])
            return data, model

        # As here the ordering isn't important (we don't know a priori which of the mismodelled features is worse) we
        # don't need to train for as long.
        self.args.epochs = 100
        self.args.nfolds = 5
        supervised_classifier(self.args, data_loader)

        # This will load the arrays that are saved at the end of supervised_training.py
        with open(self.svo.set_name('eval_info', '.np', sample_info=self.sample_info), 'rb') as file:
            y_true = np.load(file)
            y_pred = np.load(file)
            saliency = np.load(file)
            train_loss = np.load(file)
            valid_loss = np.load(file)
            shapes = np.load(file)

        # Check that the AUC of each fold is greater than 0.5
        cnt = 0
        for i, shape in enumerate(shapes):
            auc = roc_auc_score(y_true[cnt:cnt + shape], y_pred[cnt:cnt + shape])
            cnt += shape
            with self.subTest('Testing AUC of fold {i}.'):
                self.assertGreater(auc, 0.5)

        # This will load the arrays that are saved across folds in the dream.utils.evaluate_folds function
        with open(self.svo.set_name('auc', '.np', sample_info=self.sample_info), 'rb') as file:
            auc = np.load(file)
            saliency_two = np.load(file)

        with self.subTest('Testing AUC of combined folds.'):
            self.assertGreater(auc, 0.5)

        with self.subTest('Test feature ordering.'):
            self.assertCountEqual(list(np.argsort(saliency_two))[-2:], [1, 0])

        # self.end_experiment()


if __name__ == "__main__":
    unittest.main()
