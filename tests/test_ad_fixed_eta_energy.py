import os
import unittest

import numpy as np
import pandas as pd

from dream.utils.io import save_info
import torchtestcase
from dream.ad_fixed_eta_energy import get_anomaly_args, simple_anomaly_detection


class AnomalyDetectionTest(torchtestcase.TorchTestCase):

    @classmethod
    def setUpClass(cls):
        args = get_anomaly_args()
        cls.sample_info = 'ad_first'
        cls.g4_name = 'first'
        args.data_files = f'{cls.g4_name},matched'
        args.outputdir = 'unit_test'
        cls.args = args
        cls.n_samples = 20000
        cls.n_features = 5
        cls.svo = save_info(directory=cls.args.outputdir, name=cls.args.outputname)

        # This only needs to be run and trained once, all tests will just load this model and compare to a fixed
        # distribution
        cls.args.epochs = 100
        cls.args.nfolds = 3
        cls.args.load = 0
        simple_anomaly_detection(cls.args, cls.get_data_loader(cls.n_samples, cls.n_features))

    def end_experiment(self):
        os.remove(self.svo.get_image_dir())

    @staticmethod
    def get_data_loader(n_samples, n_features):
        def dl(name, **kwargs):
            model = name.split('/')[-1]
            data = pd.DataFrame(data=np.random.normal(size=(n_samples, n_features)),
                                columns=np.arange(0, n_features),
                                index=np.arange(0, n_samples))
            return data, model
        return dl

    def data_loader(self, name, **kwargs):
        return self.get_data_loader(self.n_samples, self.n_features)(name, **kwargs)

    def test_g4_vs_g4(self):
        """Check to see that the training distribution isn't identified as being anomalous.
            These were already generated.
        """
        with open(self.svo.set_name('auc', '.np', sample_info='matched'), 'rb') as file:
            auc = np.load(file)

        with self.subTest('Cannot separate distribution from itself.'):
            self.assertAlmostEqual(auc, 0.5, places=2)

    def test_ordering_easy_shifts(self):
        """Check to see that the classifier can separate a normal distribution from a shifted normal distribution,
        and gets the ordering correct for shifted distributions.
        """

        def data_loader(name, **kwargs):
            model = name.split('/')[-1]
            data, _ = self.data_loader(name)
            if model[0] == 's':
                shift = int(model[1])
                # Shift the gaussian along every axis by the same amount
                data.iloc[:, :shift] += 3 * np.ones(shift)
            return data, model

        ordered_samples = [f's{i}' for i in range(1, self.n_features)]
        self.args.data_files += ',' + ','.join(ordered_samples)

        # Evaluate each of the ordered samples using the already trained anomaly detection method
        simple_anomaly_detection(self.args, data_loader)

        # Load each of the distributions
        ordered_aucs = []
        for sample_info in ordered_samples:
            with open(self.svo.set_name('auc', '.np', sample_info=sample_info), 'rb') as file:
                auc = np.load(file)

            ordered_aucs += [auc]

            with self.subTest(f'Testing AUC of combined folds for {sample_info}.'):
                self.assertGreater(auc, 0.5)
        ordered_aucs = np.array(ordered_aucs)

        with self.subTest('Order is assigned correctly.'):
            np.testing.assert_array_equal(ordered_aucs, sorted(ordered_aucs))

    def test_correlationss(self):
        """
        Check to see that the classifier can separate an uncorrelated normal distribution from a correlated normal.
        """

        sample_info = 'correlated'
        def data_loader(name, **kwargs):
            model = name.split('/')[-1]
            data, _ = self.data_loader(name)
            if model == sample_info:
                # Sort the first and second axes, don't change 1D hist, but do change higher dim correlations
                data.iloc[:, 0] = np.sort(data.iloc[:, 0])
                data.iloc[:, 1] = np.sort(data.iloc[:, 1])
            return data, model

        self.args.data_files = f'{self.g4_name},{sample_info}'

        # Evaluate each of the ordered samples using the already trained anomaly detection method
        simple_anomaly_detection(self.args, data_loader)
        with open(self.svo.set_name('auc', '.np', sample_info=sample_info), 'rb') as file:
            auc = np.load(file)

        with self.subTest(f'Testing AUC of combined folds for {sample_info}.'):
            self.assertGreater(auc, 0.5)

        # self.end_experiment()


if __name__ == "__main__":
    unittest.main()
