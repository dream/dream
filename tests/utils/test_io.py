"""Tests for the I/O utility functions."""
import argparse
import os
import tempfile
import unittest

import torch
import numpy as np

from dream.utils import io
from dream.utils.io import save_info
import torchtestcase


class GetFilesTest(unittest.TestCase):

    def setUp(self):
        self.count = 0

    def make_file(self, directory):
        open(f'{directory}/particle_energy{self.count}_eta_G4.root', 'a').close()
        open(f'{directory}/particle_energy{self.count}_eta_FCS.root', 'a').close()
        self.count += 1

    def test_default_pairs(self):
        def make_file(directory):
            open(f'{directory}/particle_energy{self.count}_eta_G4.root', 'a').close()
            open(f'{directory}/particle_energy{self.count}_eta_FCS.root', 'a').close()
            self.count += 1

        n_files = 10
        with tempfile.TemporaryDirectory() as data_dir:
            for i in range(n_files):
                make_file(data_dir)
            pairs = io.get_files(data_dir, pair=True)
            self.assertEqual(len(pairs), n_files)

    def test_default_groups(self):
        def make_file(directory):
            open(f'{directory}/particle_eta_energy_G4.root', 'a').close()
            open(f'{directory}/particle_eta_energy_FCS{self.count}.root', 'a').close()
            self.count += 1

        n_files = 10
        with tempfile.TemporaryDirectory() as data_dir:
            for i in range(n_files):
                make_file(data_dir)
            pairs = io.get_files(data_dir, pair=False)
            self.assertEqual(len(pairs), 1)

    def test_multiple_directories(self):
        def make_file(geant4_dir, fcs_dir):
            open(f'{geant4_dir}/particle_eta_energy_G4.root', 'a').close()
            open(f'{fcs_dir}/particle_eta_energy_FCS{self.count}.root', 'a').close()
            self.count += 1

        n_files = 10
        with tempfile.TemporaryDirectory() as geant4_dir, tempfile.TemporaryDirectory() as fcs_dir:
            for i in range(n_files):
                make_file(geant4_dir, fcs_dir)
            pairs = io.get_files([geant4_dir, fcs_dir], pair=True)
            self.assertEqual(len(pairs), n_files)



class IoTest(torchtestcase.TorchTestCase):
    def test_save_and_load_tensor(self):
        x = torch.randn(10, 1000)
        filename = 'tests/utils/random.pt'
        io.save_tensor(x, filename)
        self.assertTrue(os.path.isfile(filename))
        y = io.load_tensor(filename)
        torch.testing.assert_allclose(x, y)
        os.remove(filename)

    def test_get_sim_info(self):
        filename = f'photon_262_2_nm'
        p, en, et, nm = io.get_properties(filename)
        self.assertEqual(f'{p}_{en}_{et}_{nm}', filename)


class SaveInfoTest(torchtestcase.TorchTestCase):

    def setUp(self):
        self.svo = save_info(name='test', directory='tests')
        self.t_nm = 'tests/test'
        parser = argparse.ArgumentParser()
        parser.add_argument('--one', type=str, default='1')
        parser.add_argument('--two', type=int, default=2)
        self.args = parser.parse_args()

    def test_get_image_dir(self):
        try:
            with tempfile.TemporaryDirectory() as data_dir:
                im_dir = self.svo.get_image_dir()
                open(os.path.join(data_dir, im_dir), 'a')
        except Exception as e:
            self.fail("get_image_dir does not raises an exception unexpectedly when using output to create dir.")

    def test_set_name(self):
        d_nm = 'test'
        nm = self.svo.set_name(d_nm)
        true_name = f'{io.get_top_dir()}/images/{self.t_nm}_{d_nm}'
        self.assertEqual(nm, true_name)

    def test_read_and_register(self):
        self.svo.register_experiment(self.args)
        loaded_args = self.svo.read_experiment()
        self.assertEqual(vars(self.args), loaded_args)


if __name__ == "__main__":
    unittest.main()
