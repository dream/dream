"""Tests for the PyTorch utility functions."""

import unittest

import torch
import numpy as np

from dream.utils import torch_utils
import torchtestcase


class TorchUtilsTest(torchtestcase.TorchTestCase):
    def test_tensor2numpy(self):
        x = torch.randn(10, 3)
        numpy_x = torch_utils.tensor2numpy(x)
        with self.subTest('Tensor to numpy succeeds.'):
            self.assertIsInstance(numpy_x, np.ndarray)

        numpy_x = np.zeros((10, 3))
        with self.subTest('Numpy to numpy does not succeed.'):
            with self.assertRaises(Exception):
                torch_utils.tensor2numpy(numpy_x)

    def test_make_labels(self):
        x = np.zeros((10, 3))
        y = np.zeros((10, 3))
        labels = torch_utils.make_labels(x, y)
        with self.subTest('Correct batch dimensions.'):
            self.assertEqual(labels.shape[0], 20)
        with self.subTest('Correct number of labels and correct assignment.'):
            self.assertEqual(sum(labels), 10)

    def test_to_numpy(self):
        x = np.zeros((10, 3))
        with self.subTest('Numpy to numpy array suceeds.'):
            self.assertIsInstance(torch_utils.to_numpy(x), np.ndarray)
        x = torch.randn(10, 3)
        with self.subTest('Torch to numpy array succeeds.'):
            self.assertIsInstance(torch_utils.to_numpy(x), np.ndarray)
        x = list(range(10))
        with self.subTest('List to numpy array.'):
            self.assertIsInstance(torch_utils.to_numpy(x), np.ndarray)
        with self.subTest('Dictionary to numpy fails.'):
            with self.assertRaises(Exception):
                x = {'a': 0}
                torch_utils.to_numpy(x)

    def test_to_tensor(self):
        x = np.zeros((10, 3))
        with self.subTest('Numpy to tensor.'):
            self.assertIsInstance(torch_utils.to_tensor(x), torch.Tensor)
        x = torch.randn(10, 3)
        with self.subTest('Tensor to tensor.'):
            self.assertIsInstance(torch_utils.to_tensor(x), torch.Tensor)
        x = list(range(10))
        with self.subTest('List to tensor.'):
            self.assertIsInstance(torch_utils.to_tensor(x), torch.Tensor)
        with self.subTest('Dict to tensor fails.'):
            with self.assertRaises(Exception):
                x = {'a': 0}
                torch_utils.to_tensor(x)


if __name__ == "__main__":
    unittest.main()
