"""Tests for the PyTorch utility functions."""
import os
import unittest

import torch
import numpy as np

from dream.utils import plotting
import torchtestcase

from dream.utils.torch_utils import tensor2numpy


class PlottingTest(torchtestcase.TorchTestCase):
    def test_get_bins(self):
        x_nd = torch.randn(10, 3)
        with self.assertRaises(Exception):
            plotting.get_bins(x_nd)

        def test_floats(x_nd):
            n_bins = 20
            bins = plotting.get_bins(x_nd, n_bins)
            with self.subTest('Check bin output is a numpy array.'):
                self.assertIsInstance(bins, np.ndarray)
            with self.subTest('Check bin output is correct size.'):
                self.assertEqual(len(bins), n_bins)

        test_floats(torch.randn(1, 1000))
        test_floats(np.random.randn(1, 1000))

        def test_integers(x_nd):
            bins = plotting.get_bins(x_nd, nbins=10)
            with self.subTest('Check bin output size is ignored for integers.'):
                self.assertEqual(len(bins), 21)
            with self.subTest('Check correct bins have been chosen.'):
                self.assertTrue((bins[1:] - np.unique(x_nd) > 0).sum())

        test_integers(np.random.randint(0, 20, 10000).astype('float32'))
        test_integers(torch.randint(0, 20, (10000,)).type(torch.float32))

    def test_plot_roc(self):
        n_samples = 100000
        y_true = np.random.randint(0, high=2, size=n_samples)
        y_random = np.random.randint(0, high=2, size=n_samples)
        filename = 'tests/utils/random.png'
        auc = plotting.plot_roc(y_true, y_random, filename, 'Title', add_legend=True)
        with self.subTest('Check ROCAUC value is correct.'):
            np.testing.assert_allclose(auc, 0.5, rtol=0.01)
        with self.subTest('Check figure has been created.'):
            self.assertTrue(os.path.isfile(filename))
        auc = plotting.plot_roc(y_true, y_true, filename, 'Title', add_legend=True)
        with self.subTest('Perfect separation ROCAUC.'):
            np.testing.assert_allclose(auc, 1.0, rtol=0.01)
        os.remove(filename)

    def test_get_salience(self):
        n_features = 10
        x_nd = torch.randn(1, n_features, requires_grad=True)
        y = x_nd ** 2
        derivative = tensor2numpy(2 * x_nd.abs())
        saliency = plotting.get_salience(x_nd, y)
        np.testing.assert_allclose(saliency, derivative, rtol=0.00001)


if __name__ == "__main__":
    unittest.main()
