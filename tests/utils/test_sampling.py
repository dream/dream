"""Tests for the data loaders."""
import unittest

import pandas as pd
import numpy as np

import torchtestcase

from dream.utils.sampling import AnomalyGenerator


class SamplingTest(torchtestcase.TorchTestCase):

    def setUp(self):
        features = list('ABCD')
        self.n_samples = 1000
        data = np.concatenate((
            np.random.randn(self.n_samples, len(features) - 1),
            np.random.randint(0, 10, (self.n_samples, 1))
        ), 1)
        self.data = pd.DataFrame(data, columns=features)
        self.labels = np.random.randint(0, 1, size=(self.n_samples, 1)).astype(np.float32)
        self.n_mix = int(self.n_samples / 2)

    def ad_gen_test_type(self, data, input_dtype):
        generator = AnomalyGenerator(data)
        with self.subTest('Integers have been identified correctly.'):
            np.testing.assert_array_equal(generator.int_mask, np.array([0., 0., 0., 1.]))
        with self.subTest('Correct number of samples generated.'):
            self.assertEqual(generator(self.data.shape[0] - 10).shape[0], self.data.shape[0] - 10)
        self.assertEqual(generator(self.data.shape[0] + 10).shape[0], self.data.shape[0] + 10)

        false_max_vals = np.array(self.data.max(0) + 1000)
        samples = generator(2000, max_val=false_max_vals)
        with self.subTest('Passing a maximum value is respected.'):
            self.assertTrue(np.allclose(samples.max(0), false_max_vals, rtol=0.01, atol=0))

        false_min_vals = np.array(self.data.min(0) - 1000)
        samples = generator(2000, min_val=false_min_vals)
        with self.subTest('Passing a minimum value is respected.'):
            self.assertTrue(np.allclose(samples.min(0), false_min_vals, rtol=0.01, atol=0))

        samples = generator(2000, shuffle=0)
        with self.subTest('Data is really mixed.'):
            np.testing.assert_array_equal(samples[-self.data.shape[0]:],
                                          self.data.to_numpy().astype(input_dtype))

        with self.subTest('Sampler returns data of the same type'):
            self.assertEqual(input_dtype, samples.dtype)

    def test_anomaly_generator_pandas(self):
        self.ad_gen_test_type(self.data, np.float64)

    def test_anomaly_generator_numpy(self):
        self.ad_gen_test_type(self.data.to_numpy().astype('float32'), np.float32)


if __name__ == "__main__":
    unittest.main()
