import unittest

import tempfile
from glob import glob

import numpy as np

from dream.supervised_classifier import get_supervised_args

from dream.utils.io import save_info
from dream.utils import io

from dream.reports.generate_report import generate_meta_report
from dream.reports import generate_report
import matplotlib.pyplot as plt
import os


class MyTestCase(unittest.TestCase):
    def setUp(self):
        self.count = 0
        self.args = get_supervised_args()

    def test_get_experiments(self):
        n_files = 10

        def make_files(directory, i):
            if i > n_files // 2:
                directory += f'/{i}'
                os.makedirs(directory, exist_ok=True)
            open(f'{directory}/particle_eta_energy_FCS{i}.json', 'a').close()

        with tempfile.TemporaryDirectory() as directory:
            for i in range(n_files):
                make_files(directory, i)
            dirs = generate_report.get_experiments(directory)
            with self.subTest('Can find files across two levels.'):
                self.assertEqual(len(dirs), n_files)

        with tempfile.TemporaryDirectory() as directory:
            dirs = generate_report.get_experiments(directory)
            with self.subTest('Does not find any files if there are not any.'):
                self.assertEqual(len(dirs), 0)

    def test_get_info(self):
        n_files = 10

        def make_file(directory, i):
            args = self.args
            args.outputdir = directory
            args.outputname = i
            args.data_files = f'particle_eta_energy_G4,particle_eta_energy_FCS{self.count}'
            data_files = args.data_files.split(',')
            svo = save_info(directory=args.outputdir, name=args.outputname)
            svo.register_experiment(args)
            fig, ax = plt.subplots(1, 10, figsize=(20, 8))
            fig.savefig(svo.set_name(f'features_{data_files[1]}'))
            with open(svo.set_name('auc', '.np', sample_info=data_files[1]), 'wb') as file:
                np.save(file, i / n_files)
            self.count += 1

        # with tempfile.TemporaryDirectory() as data_dir:
        data_dir = 'test_report'
        for i in range(n_files):
            make_file(f'{data_dir}', i)
        test_dir = os.path.join(io.get_top_dir(), 'images')
        directory = os.path.join(test_dir, data_dir)
        dirs = glob(f'{directory}/**/*.json')
        info = generate_report.get_info(dirs, True)
        with self.subTest('Gets the right number of plots'):
            self.assertEqual(len(info['eta']), n_files)
        correct_names = {'particle_type': 'particle', 'eta': 'eta', 'energy': 'energy'}
        for property in correct_names:
            with self.subTest(f'Gets the right {property}.'):
                self.assertEqual(set(info[property]), set([correct_names[property]]))

        with self.subTest('Gets all of the figures with correct names.'):
            self.assertCountEqual(
                [os.path.join(directory, f'{i}/features_particle_eta_energy_FCS{i}.png') for i in range(n_files)],
                info['features_images']
            )

        with self.subTest('Gets all of the aucs with correct names.'):
            self.assertCountEqual([i / n_files for i in range(n_files)], info['auc'])

    def test_default_groups(self):
        n_files = 10
        def make_file(directory, i):
            args = self.args
            args.outputdir = directory
            args.outputname = i
            args.data_files = f'particle_eta_energy_G4,particle_eta_energy_FCS{self.count}'
            data_files = args.data_files.split(',')
            svo = save_info(directory=args.outputdir, name=args.outputname)
            svo.register_experiment(args)
            fig, ax = plt.subplots(1, 10, figsize=(20, 8))
            fig.savefig(svo.set_name(f'features_{data_files[1]}'))
            plt.close(fig)
            # Make a fake hessian matrix
            fig, ax = plt.subplots(1, 1, figsize=(5, 5))
            fig.savefig(svo.set_name('hessian', '.png', sample_info=data_files[1]))
            plt.close(fig)
            with open(svo.set_name('auc', '.np', sample_info=data_files[1]), 'wb') as file:
                np.save(file, i / n_files)
            self.count += 1

        # with tempfile.TemporaryDirectory() as data_dir:
        data_dir = 'test_report'
        # Generate a fake auc array to check the ordering is done correctly
        for i in range(n_files):
            make_file(f'{data_dir}', i)
        test_dir = io.get_top_dir()
        generate_meta_report(os.path.join(test_dir, 'images', data_dir), add_hessians=True)


if __name__ == '__main__':
    unittest.main()
