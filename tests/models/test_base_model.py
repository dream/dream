"""Tests for the base class through the classifier which inherits from it."""
import os
import unittest
from copy import deepcopy

import tempfile
import torch
import torchtestcase

from dream.models.nn.dense_nets import MLP

from dream.models import Classifier


class BaseModelTest(torchtestcase.TorchTestCase):

    def setUp(self):
        self.base_model = MLP
        self.n_features = 5
        self.n_classes = 1
        self.exp_name = 'test'
        self.directory = 'tests/models/'
        self.activation = torch.sigmoid
        self.n_samples = 1000
        self.loss_name = 'test'
        self.x_nd = torch.randn((self.n_samples, self.n_features))

    def get_classifier_instance(self):
        return Classifier(self.base_model, self.n_features, self.n_classes, self.exp_name,
                          directory=self.directory, activation=self.activation, loss_name=self.loss_name)

    def test_save(self):
        classifier = self.get_classifier_instance()
        with tempfile.TemporaryDirectory() as directory:
            model_save_name = f'{directory}/{self.exp_name}'
            classifier.save(model_save_name)
            self.assertTrue(os.path.isfile(model_save_name))

    def test_load(self):
        classifier = self.get_classifier_instance()
        classifier_clone = deepcopy(classifier)
        with tempfile.TemporaryDirectory() as directory:
            model_save_name = f'{directory}/{self.exp_name}'
            classifier.save(model_save_name)
            classifier = self.get_classifier_instance()
            classifier.load(model_save_name)
            for params1, params2 in zip(classifier.parameters(), classifier_clone.parameters()):
                with self.subTest('All parameters changed by loading.'):
                    self.assertTrue(params1.data.ne(params2.data).sum() == 0)
            os.remove(model_save_name)


if __name__ == "__main__":
    unittest.main()
