"""Tests for the PyTorch utility functions."""
import unittest

import numpy as np
import torch
import torchtestcase

from dream.models.nn.dense_nets import MLP

from dream.models import Classifier

from dream.models.base_model import BaseModel


class ClassifierTest(torchtestcase.TorchTestCase):

    def setUp(self):
        self.base_model = MLP
        self.n_features = 5
        self.n_classes = 1
        self.exp_name = 'test'
        self.directory = 'tests/models/'
        self.activation = torch.sigmoid
        self.n_samples = 1000
        self.x_nd = torch.randn((self.n_samples, self.n_features))

    def get_classifier_instance(self):
        return Classifier(self.base_model, self.n_features, self.n_classes, self.exp_name,
                          directory=self.directory, activation=self.activation)

    def test_constructor(self):
        classifier = self.get_classifier_instance()
        self.assertIsInstance(classifier, BaseModel)

    def test_forward(self):
        classifier = self.get_classifier_instance()
        bs = 1000
        correct_output = classifier(torch.randn((bs, self.n_features)))
        with self.subTest('Correct batch dimension.'):
            self.assertEqual(correct_output.shape[0], bs)
        with self.subTest('Correct output dimension.'):
            self.assertEqual(correct_output.shape[1], self.n_classes)
        with self.subTest('Correct output type.'):
            self.assertIsInstance(correct_output, torch.Tensor)

        with self.subTest('Oversized input breaks.'), self.assertRaises(Exception):
            classifier(torch.randn((1000, self.n_features + 1)))
        with self.subTest('Undersized input breaks.'), self.assertRaises(Exception):
            classifier(torch.randn((1000, self.n_features - 1)))
        with self.subTest('Empty list breaks.'), self.assertRaises(Exception):
            classifier([])
        with self.subTest('Empty array breaks.'), self.assertRaises(Exception):
            classifier(np.empty())
        with self.subTest('Empty tensor breaks.'), self.assertRaises(Exception):
            classifier(torch.empty())

    def test_get_scores(self):
        classifier = self.get_classifier_instance()
        scores = classifier.get_scores(self.x_nd)
        self.assertEqual(classifier(self.x_nd), self.activation(scores))

    def test_compute_loss(self):
        classifier = self.get_classifier_instance()
        labels = torch.randint(0, 2, (self.n_samples, 1)).type(torch.float32)
        weights = torch.ones((self.n_samples, 1))
        loss = classifier.compute_loss((self.x_nd, labels, weights))
        with self.subTest('Model can be differentiated.'):
            self.assertTrue(loss.requires_grad)
        loss.backward()
        parameters = classifier.parameters()
        for param in parameters:
            # Check that every parameter in the model will be updated while training.
            with self.subTest('Check all params updated.'):
                self.assertTrue(param.grad is not None)


if __name__ == "__main__":
    unittest.main()
