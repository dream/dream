"""Tests for the PyTorch utility functions."""
import unittest

import torch
import numpy as np
import torchtestcase

from dream.models.nn.dense_nets import MLP

from dream.utils.torch_utils import tensor2numpy, to_tensor


class MlpTest(torchtestcase.TorchTestCase):

    def setUp(self):
        self.n_features = 5
        self.n_classes = 1
        self.n_samples = 1000
        self.x_nd = torch.randn((self.n_samples, self.n_features))

    def get_classifier_instance(self):
        return MLP(self.n_features, self.n_classes)

    def test_forward(self):
        base_model = self.get_classifier_instance()
        pred = base_model(self.x_nd)
        with self.subTest('Output type correct.'):
            self.assertIsInstance(pred, torch.Tensor)
        with self.subTest('Output shape correct.'):
            self.assertEqual(self.n_samples, pred.shape[0])
        with self.subTest('Derivatives can be taken wrt input.'):
            self.assertTrue(pred.requires_grad)
        pred.sum().backward()
        parameters = base_model.parameters()
        for param in parameters:
            with self.subTest('Check that every parameter in the model will be updated while training.'):
                self.assertTrue(param.grad is not None)

    def test_predict(self):
        base_model = self.get_classifier_instance()

        def test_type(data):
            with torch.no_grad():
                preds_unbatched = base_model(to_tensor(data, dtype=torch.float32))
                preds_batched = base_model.predict(data, batch_size=100)
                with self.subTest('Check batch prediction works.'):
                    self.assertTrue(torch.allclose(preds_unbatched, preds_batched, rtol=1e-6))
                unbatched = base_model.predict(data)
                with self.subTest('Check predict method works.'):
                    self.assertTrue(torch.allclose(preds_unbatched, unbatched, rtol=1e-6))

        # Tensor
        test_type(self.x_nd)
        # Numpy
        test_type(np.zeros((self.n_samples, self.n_features)))
        # List
        with self.subTest('Lists cannot be passed.'):
            with self.assertRaises(Exception):
                test_type([[0] * self.n_features] * self.n_samples)


if __name__ == "__main__":
    unittest.main()
