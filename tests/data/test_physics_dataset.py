"""Tests for the data classes."""
import unittest
from copy import deepcopy

import pandas as pd
import numpy as np
import torch

import torchtestcase
import uproot
from dream.data import physics_datasets

from dream.data.data_loaders import get_data, load_ntuple
from dream.utils.sampling import AnomalyGenerator

from dream.utils.torch_utils import to_numpy


class PhysicsDatasetTest(torchtestcase.TorchTestCase):

    def setUp(self):
        self.features = list('ABCD')
        self.n_samples = 1000
        self.train_data = pd.DataFrame(np.random.randn(self.n_samples, len(self.features)), columns=self.features)
        self.labels = np.random.randint(0, 1, size=(self.n_samples, 1)).astype(np.float32)

        # Make some validation data and shift it to make sure the test will really be applied.
        self.valid_data = pd.DataFrame(np.random.randn(self.n_samples, len(self.features)) + 1, columns=self.features)
        self.valid_labels = np.random.randint(0, 1, size=(self.n_samples, 1)).astype(np.float32)

    def test_get_preprocessing_info(self):
        # Test on a specific class instance that the base class method works as desired.
        normalised_data = physics_datasets.NormalisedData(self.train_data, self.labels, quantile=0)
        vals = [self.train_data.min().to_numpy(), self.train_data.max().to_numpy()]
        lst = normalised_data.get_preprocessing_info()
        with self.subTest('Preprocessing available attribute set correctly.'):
            self.assertTrue(normalised_data.preprocessing_info_available)
        for i in range(2):
            with self.subTest('Returning correct preprocessing information.'):
                np.testing.assert_allclose(to_numpy(lst[i]), vals[i])

    def test_nan_handling(self):
        # Test on a specific class instance that the base class works as desired.
        n_features = len(self.features)
        train_data = pd.DataFrame(np.random.randn(self.n_samples, n_features), columns=self.features)
        n_nan_rows = 10
        train_data.iloc[:n_nan_rows] *= np.nan
        normalised_data = physics_datasets.NormalisedData(train_data, self.labels, quantile=0)
        with self.subTest('Base class removes NaNs.'):
            self.assertFalse(torch.isnan(normalised_data.data).sum())
        with self.subTest('Base class removes NaN correctly.'):
            self.assertCountEqual(normalised_data.data.shape, [self.n_samples - n_nan_rows, n_features])

        normalised_data.data[:n_nan_rows] *= np.nan
        normalised_data.dropna()
        with self.subTest('Class removes NaNs from data attribute.'):
            self.assertCountEqual(normalised_data.data.shape, [self.n_samples - 2 * n_nan_rows, n_features])

        train_data *= np.nan
        with self.subTest('All NaNs raises an exception'):
            with self.assertRaises(Exception):
                normalised_data = physics_datasets.NormalisedData(train_data, self.labels, quantile=0)

    def test_empty_dataframe(self):
        # Test on a specific class instance that the base class works as desired.
        train_data = pd.DataFrame()
        with self.assertRaises(Exception):
            normalised_data = physics_datasets.NormalisedData(train_data, self.labels, quantile=0)

    def correct_normaliser(self, tensor):
        """A function to check the normalisation. This will just check the values are in the right range."""
        max_values = tensor.max(0)[0]
        with self.subTest('Preprocessed tensor bounded from above.'):
            torch.allclose(max_values, torch.ones_like(max_values))
        with self.subTest('Preprocessed tensor bounded from below.'):
            torch.allclose(tensor.min(0)[0], torch.zeros_like(max_values))

    def test_update_with_generation(self):
        # Test on a specific class instance that the base class method works as desired.

        # Check that calling this method without a generator does nothing.
        data_class = physics_datasets.NormalisedData(self.train_data, self.labels, quantile=0)
        og_data = deepcopy(data_class.data)
        data_class.update_with_generation()
        with self.subTest('Calling update data with a generator does nothing.'):
            self.assertTrue(torch.allclose(data_class.data, og_data))

        # Check that calling with a generator does what it should.
        n_mix = int(self.n_samples / 10)
        data_to_mix = self.train_data.iloc[:n_mix]
        data = self.train_data.iloc[n_mix:]
        anomaly_generator = AnomalyGenerator(data_to_mix)
        data_class = physics_datasets.NormalisedData(data, anomaly_generator, quantile=0)

        # Check that the preprocessing is still done correctly
        with self.subTest('Preprocessing of the data is done correctly when generating.'):
            self.class_inst_test(data_class, self.correct_normaliser, max_take=data.shape[0])
        # Check the method itself
        data_class.preprocess()
        data_class.update_with_generation()
        with self.subTest('Update data has expected number of samples.'):
            self.assertEqual(data_class.data.shape[0], data.shape[0] * 2)
        with self.subTest('Updated data has expected number of features.'):
            self.assertEqual(data_class.data.shape[1], data.shape[1])
        with self.subTest('Updated data has the correct type.'):
            self.assertEqual(data_class.data.dtype, torch.float32)

    def class_inst_test(self, train_data, train_data_truth, valid_data=None, max_take=None):
        """
        Apply a standard non-class-specific test to a data class. This will make sure it has been integrated correctly
        with the base class.
        :param train_data: A data class whose preprocessing method will be tested.
        :param train_data_truth: Function to test properties of scaled data.
        :param valid_data: Data that will be preprocessed using the quantities calculated by train_data
        :param max_take: the maximum number of elements to take for a given comparison.
        :return: 0
        """
        # Save data to make sure that when unscaling we arrive to the same point
        og_data = deepcopy(train_data.data)

        # Preprocess the data
        train_data.preprocess()
        with self.subTest('Passed data has not already been scaled.'):
            self.assertFalse(torch.allclose(og_data.data, train_data.data[:max_take]))
        # Check data passes explicit checks
        train_data_truth(train_data.data[:max_take])
        with self.subTest('Preprocessed flag has been set correctly.'):
            self.assertTrue(train_data.preprocessed)

        # Invert the preprocessing
        train_data.reverse_preprocessing()
        with self.subTest('Inverse preprocessing has been done correctly by comparing with the original data.'):
            self.assertTrue(torch.allclose(train_data.data[:max_take], og_data, rtol=0.0, atol=1e-5))
        # Again check this property is set correctly
        with self.subTest('Preprocessed flag has been set correctly after reversing preprocessing.'):
            self.assertFalse(train_data.preprocessed)

        if valid_data is not None:
            # Test that passing information from a different dataset will result in the correct processing. This is
            # useful for scaling the validation set according to quantities calculated on the training set.
            valid_data.preprocess(train_data.get_preprocessing_info())
            with self.subTest('Preprocessing correctly applied to the validation data.'):
                self.assertCountEqual(valid_data.get_preprocessing_info(), train_data.get_preprocessing_info())
            # Check that the preprocessed flag has been set correctly
            with self.subTest('Preprocessed flag has been set correctly on validation data.'):
                self.assertTrue(valid_data.preprocessed)

        train_data._apply_preprocessing()
        with self.subTest('Abstract method does not change flag.'):
            self.assertFalse(train_data.preprocessed)
        train_data._invert_preprocessing()
        with self.subTest('Abstract inverse method does not change flag.'):
            self.assertFalse(train_data.preprocessed)
        return 0

    def instantiate_and_run_class_test(self, data_class, correct_preprocessor, **kwargs):
        """
        Instantiate the classes and pass them to the tests for each class instance. Any keyword args will be passed to
        the constructor.
        :param data_class: An function call that will return a class
        :param correct_preprocessor: a function that will check boundaries of the data
        :return:
        """
        # Define both a train and validation set
        train_data = data_class(self.train_data, self.labels, **kwargs)
        valid_data = data_class(self.valid_data, self.valid_labels, **kwargs)

        # Run the test
        self.class_inst_test(train_data, correct_preprocessor, valid_data)

        train_data = data_class(self.train_data, self.labels, **kwargs)
        train_data.data = torch.zeros_like(train_data.data)
        train_data.preprocess()
        with self.subTest('Dataclass can handle all zeros'):
            self.assertFalse(train_data.check_for_nans())

        train_data = data_class(self.train_data, self.labels, **kwargs)
        train_data.data = torch.ones_like(train_data.data)
        train_data.preprocess()
        with self.subTest('Dataclass can handle all ones'):
            self.assertFalse(train_data.check_for_nans())

        train_data.reverse_preprocessing()
        train_data.preprocessing_info_available = False
        train_data.data[:, -1] *= 1e6
        train_data.preprocess()
        with self.subTest('Dataclass can handle large values.'):
            self.assertFalse(train_data.check_for_nans())

    def test_normalised_data(self):

        self.instantiate_and_run_class_test(physics_datasets.NormalisedData, self.correct_normaliser, quantile=0)

    def test_standardised_data(self):

        def correct_standardiser(tensor):
            """A function to check the standardisation."""
            std, means = torch.std_mean(tensor, dim=0)
            with self.subTest('Mean shifted to zero.'):
                torch.allclose(means, torch.zeros_like(means))
            with self.subTest('Standard deviation scaled to one.'):
                torch.allclose(std, torch.ones_like(means))

        self.instantiate_and_run_class_test(physics_datasets.StandardisedData, correct_standardiser)


if __name__ == "__main__":
    unittest.main()
