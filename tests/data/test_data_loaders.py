"""Tests for the data loaders."""
import unittest
import pandas as pd
import numpy as np

import torchtestcase
import uproot

from dream.data.data_loaders import get_data, load_ntuple


class DataLoaderTest(torchtestcase.TorchTestCase):

    def setUp(self):
        self.file = 'tests/data/test.root'
        features = list('ABCD')
        self.data = pd.DataFrame(np.random.randn(1000, len(features)), columns=features)

    def tearDown(self) -> None:
        pass # Or just use tempfiles to avoid this

    def test_get_data(self):
        features = list('ABCD')
        g4_data = pd.DataFrame(np.random.randn(1000, len(features)), columns=features)
        sim_data = pd.DataFrame(np.random.randn(1000, len(features) + 1), columns=features + ['E'])
        def data_loader(model, **kwargs):
            return {'g4': g4_data, 'sim': sim_data}[model], model

        g4, sim, sim_info = get_data('g4', 'sim', data_loader=data_loader)

        with self.subTest('Simulation information is extracted correctly.'):
            self.assertEqual(sim_info, 'sim')
        with self.subTest('Geant4 information extracted correctly.'):
            self.assertTrue(g4.equals(g4))
        with self.subTest('Simulation data assigned correctly.'):
            self.assertTrue(sim.equals(sim_data.iloc[:, :len(features)]))


if __name__ == "__main__":
    unittest.main()
