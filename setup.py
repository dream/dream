from setuptools import find_packages, setup

with open("README.md", "r") as fh:

    long_description = fh.read()
# Update requires is handled by docker, see base pytorch container and additional libraries.
setup(
    name="DREAM",
    version='0.01',
    description="Density Ratio Estimation for Automised Monitoring.",
    long_description=long_description,
    long_description_content_type='text/markdown',
    url="https://gitlab.cern.ch/dream/dream",
    author="Sam Klein",
    packages=find_packages(exclude=["tests"]),
    license="MIT",
    dependency_links=[],
)
