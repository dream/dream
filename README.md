# DREAM
Density Ratio Estimation for Automatised Monitoring

DREAM is a toolbox for performing automatic validation using density ratio estimators. 

The purpose of the tool is to distinguish between 'true' samples from the true data distribution (Geant4) and 'fast' 
samples from some approximation to the true distribution that is quick to sample from.

The tool performs two steps:
 1. Anomaly detection
    1. Train one model per 'true' sample, apply model to many different 'fast' samples.
    2. Identify large differences between 'fast' samples and 'true' samples.
    3. Exclude very obviously poor models from being passed to stage 2.

 2. Supervised classification
    1. Train a supervised classifier for every 'fast' sample that is passed against the provided 'true' sample.
    2. Rank each 'fast' sample.
    3. Rank the modelling performance of the features in each individual 'fast' sample.

The tool also provides some basic reporting for comparing many jobs across multiple runs, and also scripts for handling 
submission of many jobs. Files will be automatically paired in the submission.

## Installation

Clone the git repository and corresponding docker file.

When using singularity for example

```angular2html
mkdir container
cd container
singularity pull pillow.sif docker://gitlab-registry.cern.ch/dream/dream/latest
```

## Usage

DREAM provides default data loading and naming functions, to run jobs using these defaults the directories to search
under as well as the job name and directory under which to search is required. This command needs to be run within the 
container for the time being. THe first path should point to the location of the data.

```angular2html
python -c "from dream.utils.io import launch_batch_jobs; launch_batch_jobs('/data/', 'job_name', 'job_directory')"
```

Three functions are used to customise the running of DREAM jobs, either one or all three can be changed depending on the
level of customisability required.

The function which is used to load the data from file can be defined as follows.

```angular2html
import uproot as up
def data_loader(data_file, features=None, include_integers=True):
    """
    Function for configuring the DREAM pipeline.
    :param data_file: a string pointing to a specific root file
    :param features: a list (or string separated by commas) of features to train on. In the default loader, passing
    None will take all values.
    :param include_integers: a boolean, set to True to include integer type features.
    :return: A tuple of pandas DataFrame and string for the model name (containing information on energy/eta/...
             slice)
    """
    # Get the name of the model {FCS, AF2/3, Fast Calo GAN, ....}
    model_name = data_file.split('/')[-1]

    # Define the jet algorithm and features to train with
    jet_algorithm = 'AntiKt4EMPFlowJetsAuxDyn'
    varnames = [f'{jet_algorithm}.N90Constituents', f'{jet_algorithm}.AverageLArQF']
    # Load the file
    with up.open(data_file) as f:
        data = f['CollectionTree'].arrays(varnames, library="pd")

    return data, model_name
```

The function which is used to identify the 'truth' variables can be defined as follows. In this case the truth is Geant4
and the type of the file can be taken from the file name directly. This property could also be taken from loading the file
for example.

```angular2html
def geant_4_conditions(file_name):
    """
    Function for identifying geant4 files.
    :param file_name: A string that points to a file that can be read with the already defined data_loader.
    :return:
    """
    return file_name[-6:] == 'GEANT4'
```

Another important thing to be able to customise it the different levels that define the hierarchy which splits up the
different samples. Again, in this case, the splitting can be taken directly from the string that defines the filename.

```angular2html
def get_properties(file_name):
    """
    A function for identifying the hierarchies/slices in the data.
    :param file_name: A string that labels a given data file, all directory information will have been stripped.
    :return: The three levels on which to separate the different files, and a name to tag this file with. In
    the fast calo sim case this might be the particle, energy, eta and flavor of simulation.
    """
    level_0, level_1, level_2, name = file_name.split('_')
    return level_0, level_1, level_2, name
```

Having defined these custom functions, a batch job can be launched using the following.

```angular2html
launch_supervised_batch_jobs(data_directories, data_loader=data_loader, conditions=geant_4_conditions,
                                 get_properties=get_properties)
```

This example is repeated in full in ```examples/multiple_jobs.py```.

A working example of running the supervised classifier on a single sample has been provided in the examples folder.

### HTCondor submission

The files ```run_job.sh``` and ```submit_condor.sub``` demonstrate how the tool can be run using the HT Condor submission system. On lxplus the container will need to be pulled under ```/eos/```.

## Development

To develop this project please get in touch with the maintainer.