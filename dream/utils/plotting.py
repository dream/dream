import pdb

import seaborn as sns

import pandas as pd
from sklearn.metrics import roc_curve, roc_auc_score
import matplotlib.pyplot as plt
import numpy as np
import torch

from .torch_utils import tensor2numpy, to_numpy


def plot_training(train_loss, valid_loss, save_name, title='Classifier Training'):
    plt.figure()
    plt.plot(train_loss, label='Train')
    plt.plot(valid_loss, label='Validation')
    plt.legend()
    plt.title(title)
    plt.tight_layout()
    plt.savefig(save_name)
    plt.clf()


def get_bins(data, nbins=20):
    """Bin data for histograms, if integers the number of bins arg will be ignored."""
    if len(data.squeeze().shape) > 1:
        raise Exception(f"Can't bin data of shape {data.shape}.")
    max_ent = data.max().item()
    min_ent = data.min().item()
    if (data.round() == data).prod():
        return np.arange(min_ent, max_ent + 1.5) - 0.5
    else:
        return np.linspace(min_ent, max_ent, num=nbins)


def plot_roc(y_true, y_pred, sv_nm, sample_info, add_legend=True):
    y_true = to_numpy(y_true)
    y_pred = to_numpy(y_pred)
    fig, ax = plt.subplots(1, 1, figsize=(5, 5))
    fpr, tpr, thresholds = roc_curve(y_true, y_pred)
    auc = roc_auc_score(y_true, y_pred)
    add_roc_curve(fpr, tpr, ax, auc=auc)

    ax.plot([0, 1], [0, 1], 'k--')
    ax.set_xticks(np.arange(0, 1.1, 0.1))
    ax.set_xlabel('False positive rate')
    ax.set_ylabel('True positive rate')
    ax.set_title(sample_info)
    if add_legend:
        ax.legend()
    fig.savefig(sv_nm)
    plt.close(fig)

    return auc


def compute_and_plot_roc(model, dataset, sv_nm, add_legend=True, sample_info=None):
    y_true = tensor2numpy(dataset.targets)
    y_pred = tensor2numpy(model.predict(dataset.data))
    plot_roc(y_true, y_pred, sv_nm, sample_info, add_legend)
    return y_true, y_pred


def add_roc_curve(fpr, tpr, ax, label='', auc=None):
    if auc:
        label += f'AUC: {auc:.2f}'
    ax.plot(fpr, tpr, linewidth=2, label=label)


def plot_weights_dists(test_data, predictions, svo, bins=10, sample_info=None):
    carl_weights = predictions / (1 - predictions)
    fig, ax = plt.subplots(1, test_data.nfeatures, figsize=(5 * test_data.nfeatures + 2, 5))
    for i, feature in enumerate(test_data.data.t()):
        feature = tensor2numpy(feature)
        # Sometimes the weights can be inf or NaN
        carl_weight = carl_weights[:, 0]
        carl_weight = carl_weight[np.isfinite(carl_weight)]
        # feature = feature[np.isfinite(carl_weight)]
        count, xbins, ybins = np.histogram2d(feature, carl_weight, bins=bins)
        count[count == 0] = np.nan
        ax[i].imshow(count.T,
                     origin='lower', aspect='auto',
                     extent=[xbins.min(), xbins.max(), ybins.min(), ybins.max()],
                     )
        ax[i].set_xlabel(test_data.feature_names[i])
        if i == 0:
            ax[i].set_ylabel('Weight')
    fig.suptitle(sample_info)
    fig.tight_layout()
    fig.savefig(svo.set_name('weight_distributions', '.png'))
    plt.close(fig)


def hist_salience(saliency, feature_names, nm, sample_info=None):
    n_features = saliency.shape[1]
    fig, ax = plt.subplots(1, n_features, figsize=(5 * n_features + 2, 5))
    for i in range(n_features):
        salience = saliency[:, i]
        ax[i].hist(salience)
        ax[i].set_xlabel(f'|d(prediction)/d({feature_names[i]})|')
        ax[i].set_title(f'{feature_names[i]} \n {np.mean(salience):.4f}')
    if sample_info is not None:
        fig.suptitle(sample_info)
    fig.tight_layout()
    fig.savefig(nm)
    plt.close(fig)


def get_salience(x, scores):
    """Perform the backward pass on the score"""
    scores.backward(torch.ones_like(scores))
    saliency = x.grad.abs()
    saliency = tensor2numpy(saliency)
    return saliency


def get_hessian(x, model, n_sample=1000):
    hessian = torch.zeros(x.shape[1], x.shape[1]).to(x.device)
    x = x[torch.randperm(x.shape[0])]
    for i in range(n_sample):
        hessian += torch.autograd.functional.hessian(model, x[i])
    return hessian / n_sample


def compute_and_plot_salience(model, dataset, nm, sample_info=None):
    # We want to find the gradient with respect to the given example
    x = dataset.data
    x.requires_grad_()
    if x.grad is not None:
        x.grad.zero_()
    # Get the model output pre activation
    scores = model.get_scores(x)
    saliency = get_salience(x, scores)

    # Plot the histograms of the derivatives for each feature
    hist_salience(saliency, dataset.feature_names, nm.format('hists'), sample_info)

    # Plot the derivatives of each against the prediction
    preds = tensor2numpy(model.activation(scores).flatten())
    fig, ax = plt.subplots(1, dataset.nfeatures, figsize=(5 * dataset.nfeatures + 2, 5))
    for i in range(dataset.nfeatures):
        ax[i].hist2d(saliency[:, i], preds)
        ax[i].set_xlabel(f'|d(prediction)/d({dataset.feature_names[i]})|')
        ax[i].set_ylabel('prediction')
        ax[i].set_title(dataset.feature_names[i])
    fig.suptitle(sample_info)
    fig.tight_layout()
    fig.savefig(nm.format('hists_scores'))
    plt.close(fig)

    # Plot the derivatives of each against the features themselves
    fig, ax = plt.subplots(1, dataset.nfeatures, figsize=(5 * dataset.nfeatures + 2, 5))
    dataset.reverse_preprocessing()
    x = dataset.data
    for i in range(dataset.nfeatures):
        ax[i].hist2d(tensor2numpy(x[:, i]), saliency[:, i])
        ax[i].set_xlabel(f'{dataset.feature_names[i]} eV')
        ax[i].set_ylabel(f'|d(prediction)/d({dataset.feature_names[i]})|')
        ax[i].set_title(dataset.feature_names[i])
    fig.suptitle(sample_info)
    fig.tight_layout()
    fig.savefig(nm.format('hists_features_preds'))
    plt.close(fig)

    # To avoid any confusion reapply the preprocessing
    dataset.preprocess()

    # Calculate the hessian matrix and plot is to see which values are well modelled.
    hessian = get_hessian(x, model)
    plot_hessian(hessian, dataset.feature_names, nm.format('hessian'))

    return saliency, hessian

def plot_hessian(hessian, feature_names, png_name):
    fig, ax = plt.subplots(1, 1, figsize=(5, 5))
    df = pd.DataFrame(to_numpy(hessian), columns=feature_names)
    df.to_csv(png_name.split('.')[0] + '.csv')
    # Mask the upper half of the matrix
    lr_mask = np.triu(df, 1)
    sns.heatmap(df, cmap='coolwarm', cbar=False, annot=True, fmt='.2f', ax=ax, mask = lr_mask)
    fig.savefig(png_name)
    plt.close(fig)


def get_weights(data):
    return np.ones_like(data) / len(data)


def mask_outliers(data):
    lb = np.quantile(data, 0.01)
    ub = np.quantile(data, 0.99)
    return np.clip(data, lb, ub)


def plot_features(datasets, nm, sample_info=None, mean_derivatives=None, max_columns=5, n_features=None, poster=False,
                  compare_data=None, labels=None):
    if n_features is None:
        n_features = datasets.nfeatures
    n_rows = int(np.ceil(n_features / max_columns))
    n_cols = int(min(max_columns, n_features))
    fig, ax_ = plt.subplots(n_rows, n_cols, figsize=(5 * n_cols + 2, 5 * n_rows))
    ax = fig.axes
    data, targets, feature_names = datasets.reorder_columns(mean_derivatives)
    mx = (targets == 0).flatten()
    if labels is None:
        labels = ['true', 'simulated'] if poster else ['G4', 'FCS']
    for i in range(n_rows * n_cols):
        # Set the additional canvases to be invisible
        if i < n_features:
            data_in = data[:, i][mx]
            g4_sample = mask_outliers(tensor2numpy(data_in))
            fcs_sample = mask_outliers(tensor2numpy(data[:, i][~mx]))
            bins = get_bins(np.hstack((g4_sample, fcs_sample)))
            ax[i].hist(g4_sample, label=labels[0], histtype='step', bins=bins, weights=get_weights(g4_sample))
            ax[i].hist(fcs_sample, label=labels[1], histtype='step', bins=bins, weights=get_weights(fcs_sample))
            if compare_data is not None:
                c_data = mask_outliers(compare_data[:, i])
                ax[i].hist(c_data, label=labels[2], histtype='step', bins=bins, weights=get_weights(c_data))
            # ax[i].legend()
            if mean_derivatives is not None:
                ax[i].set_title(f'Mean derivative {sorted(mean_derivatives)[::-1][i] :.2f}')
            ax[i].set_xlabel(f'{feature_names[i]}')
        else:
            ax[i].set_visible(False)
    handles, labels = ax[0].get_legend_handles_labels()
    fig.legend(handles, labels, loc='upper right', frameon=False)
    fig.suptitle(sample_info, fontsize=32)
    fig.tight_layout(rect=[0, 0.01, 1, 0.97])
    if poster:
        fig.savefig(nm, dpi=300)
    else:
        fig.savefig(nm)
    plt.close(fig)


def plot_samples(samples, inliers, nm, feature_names=None, max_columns=5, sample_info=''):
    n_features = samples.shape[1]
    n_rows = int(np.ceil(n_features / max_columns))
    n_cols = int(min(max_columns, n_features))
    fig, ax_ = plt.subplots(n_rows, n_cols, figsize=(5 * n_cols + 2, 5 * n_rows))
    ax = fig.axes
    for i in range(n_rows * n_cols):
        # Set the additional canvases to be invisible
        if i < n_features:
            data_in = samples[:, i]
            bins = get_bins(data_in)
            g4_sample = tensor2numpy(data_in)
            fcs_sample = tensor2numpy(inliers[:, i])
            ax[i].hist(g4_sample, label='Samples', histtype='step', bins=bins, weights=get_weights(g4_sample))
            ax[i].hist(fcs_sample, label='Truth', histtype='step', bins=bins, weights=get_weights(fcs_sample))
            # ax[i].legend()
            if feature_names is not None:
                ax[i].set_xlabel(f'{feature_names[i]}')
        else:
            ax[i].set_visible(False)
    handles, labels = ax[0].get_legend_handles_labels()
    fig.legend(handles, labels, loc='upper right')
    fig.suptitle(sample_info, fontsize=32)
    fig.tight_layout(rect=[0, 0.01, 1, 0.97])
    fig.savefig(nm)
    plt.close(fig)
