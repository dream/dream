import numpy as np
import pandas as pd

from dream.data.physics_datasets import is_integers


class AnomalyGenerator:
    def __init__(self, data):
        """
        :param data: a numpy or pandas array of data.
        """
        if isinstance(data, pd.DataFrame):
            data = data.to_numpy()
        self.dtype = data.dtype
        self.data = data
        self.n_mix = data.shape[0]
        self.n_features = data.shape[1]
        self.int_mask = np.array(is_integers(self.data))


    def __call__(self, num_samples, min_val=0., max_val=1., shuffle=1):
        """
        :param num_samples: The number of samples to draw.
        :param min_val: The left support of the samples to be drawn. A list, numpy array or scalar.
        :param max_val: The right support of the random samples. A list, numpy array or scalar.
        :return:  numpy array of num_samples data points with self.n_mix uniformly generated samples mixed in.
                  If num_samples < n_mix then just the data will be returned.
        """
        if num_samples > self.n_mix:
            if not isinstance(min_val, (list, np.ndarray)):
                min_val = np.array([min_val] * self.n_features)
            if not isinstance(max_val, (list, np.ndarray)):
                max_val = np.array([max_val] * self.n_features)

            num_samples -= self.n_mix
            samples = np.empty((num_samples, self.n_features), dtype='float32')
            samples[:, ~self.int_mask] = np.random.rand(num_samples, sum(~self.int_mask))
            for i, bool in enumerate(self.int_mask):
                if bool:
                    samples[:, i] = np.random.randint(0, 2, num_samples)
            samples = samples * (max_val - min_val) + min_val
            samples = np.concatenate((samples, self.data), 0)
            if shuffle:
                np.random.shuffle(samples)
            return samples.astype(self.dtype)
        else:
            samples = np.array(self.data)
            if shuffle:
                np.random.shuffle(samples)
            return self.data[:num_samples]
