import torch
import torch.optim as optim
import torch.nn as nn
from torch.nn import functional as F
from dream.data.physics_datasets import NormalisedData, StandardisedData, LogScaledData


def get_optimizer(nm, parameters, momentum=0.9, lr=0.001, wd=0.01):
    if nm.casefold() == 'sgd':
        return optim.SGD(parameters, lr=lr, momentum=momentum)
    elif nm.casefold() == 'adam':
        return optim.Adam(parameters, lr=lr)
    elif nm.casefold() == 'adamw':
        return optim.AdamW(parameters, lr=lr, wd=wd)
    else:
        raise NotImplementedError(f'No optimizer of name {nm}')


class optimizer:
    """
    Make a function that will return an optimizer object.
    """

    def __init__(self, optimizer, learning_rate, momentum, weight_decay):
        self.optimizer = optimizer
        self.learning_rate = learning_rate
        self.momentum = momentum
        self.weight_decay = weight_decay

    def __call__(self, model):
        return get_optimizer(self.optimizer, model.parameters(), lr=self.learning_rate, momentum=self.momentum,
                             wd=self.weight_decay)


activations = {
    'none': nn.Identity(),
    'relu': F.relu,
    'elu': F.elu,
    'leaky_relu': F.leaky_relu,
    'relu6': F.relu6,
    'sigmoid': torch.sigmoid,
    'tanh': torch.tanh,
    'hard_tanh': nn.Hardtanh(),
    'gelu': nn.GELU(),
    'celu': nn.CELU(),
    'selu': nn.SELU(),
    'rrelu': nn.RReLU(),
    'prelu': nn.PReLU()
}


def get_data_class(data_processing):
    # Define the data class to use with the data
    if (data_processing == 'normalize') or (data_processing == 'normalise'):
        data_class = NormalisedData
    elif (data_processing == 'standardize') or (data_processing == 'standardise'):
        data_class = StandardisedData
    elif data_processing == 'log_scaled':
        data_class = LogScaledData
    else:
        raise Exception(f'Data processing {data_processing} not recognised')

    return data_class
