import numpy as np
import torch
from torch.utils.data import Dataset


def get_device():
    return torch.device("cuda:0" if torch.cuda.is_available() else "cpu")


def tensor2numpy(x):
    return x.detach().cpu().numpy()


def to_numpy(x):
    """This function will 'safely' take either a tensor, list, or ndarry and return an ndarray."""
    if torch.is_tensor(x):
        return tensor2numpy(x)
    elif isinstance(x, np.ndarray):
        return x
    elif isinstance(x, list):
        return np.array(x)
    else:
        raise Exception(f'Datatype not recognised for {x}.')


def to_tensor(x, dtype=None):
    """This function will 'safely' take either a tensor, list, or ndarry and return a torch tensor."""
    if torch.is_tensor(x):
        return x
    elif isinstance(x, np.ndarray):
        return torch.tensor(x, dtype=dtype)
    elif isinstance(x, list):
        return torch.tensor(x, dtype=dtype)
    else:
        raise Exception(f'Datatype not recognised for {x}.')


def make_labels(g4_data, fast_data):
    labels = np.concatenate(
        (
            np.zeros(g4_data.shape[0], dtype=np.float32),
            np.ones(fast_data.shape[0], dtype=np.float32)
        ), 0)
    return labels


class DataOnly(Dataset):
    def __init__(self, data, dtype=torch.float32):
        super(DataOnly, self).__init__()
        self.data = to_tensor(data, dtype=dtype)

    def __getitem__(self, item):
        return self.data[item]

    def __len__(self):
        return self.data.shape[0]
