import io
import pathlib
import os
import json
import pdb

import torch
import numpy as np
import random

import glob

import os
import subprocess
import sys

import yaml

SEED = 42
# TODO replace this everywhere and use as a configurable
DATA_SUBFOLDER_NAME = 'images'


def set_seed(seed):
    torch.manual_seed(seed)
    np.random.seed(seed)
    random.seed(seed)


def geant_4_conditions(name):
    """
    Function for identifying geant4 files.
    :param name: A string that points to a geant4 root file.
    :return: Boolen, True if the corresponding file is a geant4 sample
    """
    return (name[:6] == 'GEANT4') or (name[:2] == 'G4') or (name[-2:] == 'G4') or (name[-6:] == 'GEANT4')


def get_properties(file):
    """
    A default function for identifying the hierarchies/slices in the data.
    :param file_name: A string that labels a given data file, all directory information will have been stripped.
    :return: The three levels on which to separate the different files, and a name to tag this file with. In
    the fast calo sim case this might be the particle, energy, eta and flavor of simulation.
    """
    splits = file.split('_')
    if len(splits) < 3:
        pdb.set_trace()
    particle, eta, energy, name = splits[:4]
    if len(splits) > 3:
        name = '_'.join(splits[3:])
    return particle, eta, energy, name


def get_files(mount_name, pair=True, particle=None, extension='*.root', get_property=None,
              conditions=None):
    """
    A function to build the strings of files on which to operate.
    :param mount_name: The directory under which the program will search for data.
    :param pair: If False then all geant4 files will be matched with all corresponding simulation files. Otherwise
                  each simulation file will be paired with the corresponding geant 4 file. Pairs is meant for use with
                  the supervised_classifier, while groups is for anomaly detection.
                  pair=False ==> strings like ['Geant4,FCS,FastCaloGAN'] will be returned.
                  pair=True ==> strings like ['Geant4,FCS', 'Geant4,FastCaloGAN'] will be returned.
    :param particle: The name of the particle type to select.
    :param extension: The file extensions to search for.
    :param get_properties: A function which takes a file name and returns the particle, eta, energy and
    identifier/simulation name.
    :return:
    """

    if get_property is None:
        get_property = get_properties
    if conditions is None:
        conditions = geant_4_conditions

    if not isinstance(mount_name, list):
        mount_name = [mount_name]
    # Grab all files under the given directories
    files = sum([glob.glob(os.path.join(mn, extension)) for mn in mount_name], [])

    # Separate out the
    files = [file.split('/')[-1] for file in files]
    names = [get_property(file)[3] for file in files]
    geant4_files = [files[i] for i, name in enumerate(names) if conditions(name)]
    FCS_files = [files[i] for i, name in enumerate(names) if not conditions(name)]
    particles = [get_property(file)[0] for file in FCS_files]
    energies = [get_property(file)[2] for file in FCS_files]
    etas = [get_property(file)[1] for file in FCS_files]

    def accept(index, file_p):
        _, eta, energy, _ = get_property(file_p)
        bool = (etas[index] == eta) and (energies[index] == energy)
        if particle is not None:
            bool = bool and (particles[index] == particle)
        return bool

    if pair:
        pairs = []
        for g4 in geant4_files:
            pairs += [f'{g4},{file}' for i, file in enumerate(FCS_files) if accept(i, g4)]
        return pairs
    else:
        couples = []
        for g4 in geant4_files:
            _, eta, energy, _ = get_property(g4)
            ad_files = ''
            for i, file in enumerate(FCS_files):
                if accept(i, g4):
                    ad_files += f'{file},'
            if len(ad_files) > 0:
                ad_files = ad_files[:-1]
            couples += [f'{g4},{ad_files}']
        return couples



def launch_batch_jobs(data_directories, job_name, job_directory, data_loader=None, conditions=None,
                      get_properties=None, add_hessians=True, supervised=True, particle=None, extension='*.root',
                      yaml_name='options'):
    # TODO pass a dict so you can completely configure the grid.py job with other more sepcific things
    config_dict = {}
    if supervised:
        experiment = 'dream/supervised_classifier.py'
    else:
        experiment = 'dream/ad_fixed_eta_energy.py'

    # Get the pairs of files and add them to the submission
    pairs = get_files(data_directories, particle=particle, get_property=get_properties, conditions=conditions,
                      extension=extension)
    config_dict['data_files'] = pairs

    # Point to the calling script to retrieve the data loader
    if data_loader is not None:
        config_py = os.path.relpath(sys._getframe(1).f_globals['__file__']).replace('/', '.').split('.py')[0]
        config_dict['data_loader_file'] = [config_py]
    else:
        config_py = None

    top_dir = get_top_dir()
    config_dir = os.path.join(top_dir, 'job_submissions')
    os.makedirs(config_dir, exist_ok=True)
    yaml_file = os.path.join(config_dir, f'{yaml_name}.yml')
    with open(yaml_file, 'w') as outfile:
        yaml.dump(config_dict, outfile, default_flow_style=False)

    # Launch jobs
    # Chain in the job submission
    if not isinstance(data_directories, list):
        data_directories = [data_directories]
    singularity_mounts = ','.join([f'{file}' for file in data_directories])
    grid_file = os.path.join(top_dir, 'grid.py')
    subprocess.run([f'python3 {grid_file} -n {job_name} -d {job_directory} '
                    f'--singularity-mounts {singularity_mounts} '
                    f'--yaml_configs {yaml_file} '
                    f'--experiment {experiment}'], shell=True)

    # # TODO: print how to chain this properly
    print(f'When the above jobs have finished running, from within the container, run \n'
          f'generate_meta_report("/{DATA_SUBFOLDER_NAME}/{job_directory}", '
          f'get_properties={config_py}) \n')
    return 0


def get_top_dir():
    return str(pathlib.Path().absolute())


def get_data_dir():
    return str(pathlib.Path().absolute())


def get_file_name(directory, file_name):
    file_info = file_name.split('/')[0]
    if directory is not None:
        file_name = directory + file_name
    return file_name, file_info.split('.root')[0]


def save_tensor(tensors, filename):
    torch.save(tensors, filename)


def load_tensor(filename):
    with open(filename, 'rb') as f:
        buffer = io.BytesIO(f.read())
    return torch.load(buffer)


def update_attribtue(args, attr, value):
    if not hasattr(args, attr):
        raise Exception(f'{attr} is not an argument of this function')
    else:
        setattr(args, attr, value)


class save_info():
    """
    This class is used to handle consistent saving/loading of data across the tool, just to ensure that naming
    conventions are fixed all in one place.
    """

    def __init__(self, model=None, start_sv=0, name='test', directory=None, extra_id=None):
        self.top_dir = get_top_dir()
        if model is not None:
            self.model = model
            self.nm = model.exp_name
            self.sv_dir = f'{self.top_dir}/{DATA_SUBFOLDER_NAME}/{model.directory}/{model.exp_name}'
            if extra_id is not None:
                self.sv_dir += f'/{extra_id}'
            if start_sv:
                self.start_saving()
        else:
            self.sv_dir = f'{self.top_dir}/{DATA_SUBFOLDER_NAME}'
            if directory is not None:
                self.sv_dir += f'/{directory}/{name}'
            self.nm = name

        if not os.path.exists(self.sv_dir):
            os.makedirs(self.sv_dir, exist_ok=True)

    def get_image_dir(self):
        return self.sv_dir

    def get_saving_dir(self, identifier=None):
        if identifier is None:
            return self.sv_dir
        else:
            directory = os.path.join(self.sv_dir, identifier)
            os.makedirs(directory, exist_ok=True)
            return directory

    def start_saving(self):
        self.model.eval()

    def set_name(self, name, extension=None, sample_info=None):
        if sample_info is not None:
            # TODO: this is specific to FCS, need to remove this
            name += '_' + sample_info.split('.root')[0].split('/')[-1]
        nm = f'{self.sv_dir}/{name}'
        if extension is not None:
            nm += extension
        return nm

    def register_experiment(self, args):
        set_seed(SEED)
        log_dict = vars(args)
        json_dict = json.dumps(log_dict, indent=1)
        with open(f"{self.sv_dir}/{self.nm}_exp.json", "w") as file_name:
            json.dump(json_dict, file_name)

    def read_experiment(self):
        with open(f"{self.sv_dir}/{self.nm}_exp.json", "r") as file_name:
            json_dict = json.load(file_name)
        return json.loads(json_dict)

    # TODO: this isn't well implemented and should be mended
    def init_from_json(self, json_file):
        self.nm = json_file.split('/')[-1].split('_exp')[0]
        self.sv_dir = json_file.split(f'{self.nm}_exp.json')[0][:-1]
        return self
