import os
import warnings

import torch

from dream.utils.hyperparameters import get_data_class
from dream.utils.plotting import plot_training

from dream.models.classifier import Classifier, load_model
from dream.utils import hyperparameters
from dream.utils.evaluate import evaluate_fold, evaluate_folds
from dream.utils.torch_utils import get_device, tensor2numpy
from dream.data.physics_datasets import NormalisedData, StandardisedData, LogScaledData
from dream.utils.io import save_info, save_tensor
from sklearn.metrics import roc_auc_score

from sklearn.model_selection import StratifiedKFold, KFold
import numpy as np


def fit_model(model, train_data, valid_data, optimizer, batch_size, n_epochs, device, plot=True, save=True,
              move_data_gpu=True, scheduler=None, identifier=0, load_best=False, debugging=False):
    # Make an object to load training data
    data_obj = torch.utils.data.DataLoader(train_data, batch_size=batch_size, shuffle=True, num_workers=0,
                                           pin_memory=move_data_gpu)
    data_valid = torch.utils.data.DataLoader(valid_data, batch_size=1000, shuffle=True, num_workers=0,
                                             pin_memory=move_data_gpu)
    # Make an object that will be used to save all images and models in this loop
    svo = save_info(model, 0)
    n_train = int(np.ceil(len(train_data) / batch_size))
    n_valid = int(np.ceil(len(valid_data) / 1000))

    if isinstance(scheduler, str):
        if scheduler == 'cosine':
            scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer,
                                                                   len(train_data) / batch_size * n_epochs)
        elif scheduler == 'plateau':
            scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, 'min')
        elif scheduler == 'none':
            scheduler = None
        else:
            raise ValueError(f'{scheduler} not a valid argument for the scheduler.')

    train_loss = np.zeros(n_epochs)
    valid_loss = np.zeros(n_epochs)
    classifier_directory = svo.get_saving_dir(f'{identifier}')
    model_save_dir = f'{classifier_directory}/saved_models'
    os.makedirs(model_save_dir, exist_ok=True)

    def nm_as(name):
        return f'{model_save_dir}/{name}'

    for epoch in range(n_epochs):  # loop over the dataset multiple times

        running_loss = np.zeros(n_train)
        for i, data in enumerate(data_obj, 0):
            # zero the parameter gradients
            optimizer.zero_grad()

            # Get the model loss
            data = [dt.to(device) for dt in data]
            loss = model.compute_loss(data)
            # Propogate the loss
            loss.backward()
            # Update the parameters
            optimizer.step()
            # Step the scheduler if one is being used
            if scheduler is not None:
                scheduler.step()

            # Get statistics
            running_loss[i] = loss.item()

        # If training with a generator object, update the data
        train_data.update_with_generation()

        # Save loss info for the epoch
        train_loss[epoch] = np.mean(running_loss)

        # Validate
        running_loss = np.zeros(n_valid)
        with torch.no_grad():
            for j, data in enumerate(data_valid, 0):
                # Get the model loss
                data = [dt.to(device) for dt in data]
                loss = model.compute_loss(data)
                running_loss[j] = loss.item()
        valid_loss[epoch] = np.mean(running_loss)
        model.save(nm_as(epoch))

    if debugging:
        with torch.no_grad():
            y_pred = model.predict(valid_data.data).view(-1).numpy()
            y_true = valid_data.targets.view(-1).numpy()
        auc = roc_auc_score(y_true, y_pred)
        from dream.utils.plotting import plot_features, get_bins
        prefix = f'{svo.sv_dir}/{identifier}'
        plot_features(valid_data, f'{prefix}_valid_data.png')
        plot_features(train_data, f'{prefix}_train_data.png')
        import matplotlib.pyplot as plt
        plt.figure()
        class_0 = y_pred[y_true == 0]
        class_1 = y_pred[y_true == 1]
        bins = get_bins(np.hstack((class_0, class_1)))
        plt.hist(class_0, bins=bins, label='class 0', alpha=0.5)
        plt.hist(class_1, bins=bins, label='class 1', alpha=0.5)
        plt.legend()
        plt.savefig(f'{prefix}_output_dist.png')
        plt.clf()

    if plot:
        plot_training(train_loss, valid_loss, f'{classifier_directory}/training.png')

    if save:
        info = train_data.get_preprocessing_info()
        save_tensor(info, f'{classifier_directory}/scaling_info')

    if load_best:
        best_epoch = np.argmin(valid_loss)
        print(f'Best epoch: {best_epoch} loaded')
        model.load(nm_as(best_epoch))

    print('Finished Training')

    return train_loss, valid_loss


def train_model_nfolds(data, labels_or_generator, model, optimizer, epochs, batch_size, nfolds=5,
                       data_processing='normalise', scheduler=None, sample_info='test',
                       evaluate=True, outputdir='classifier', outputname='test', load=False):
    # Get the device
    device = get_device()
    print(f'Currently running on {device}')

    # Check if what has been passed for labels is a generator, if so this will generate AD samples.
    is_generator = callable(labels_or_generator)
    if is_generator:
        generator = labels_or_generator
    else:
        labels = labels_or_generator

    # Define the data class to use with the data
    data_class = get_data_class(data_processing)

    nfolds = nfolds
    y_true = []
    y_pred = []
    saliency = []
    hessian = []
    train_loss = np.zeros((nfolds, epochs))
    valid_loss = np.zeros_like(train_loss)

    if nfolds == 1:
        warnings.warn('Must run on more than one fold. Increasing fold number to 2.')
        nfolds = 2
    if is_generator:
        kfold = KFold(n_splits=nfolds, shuffle=True, random_state=1)
        split_inds = kfold.split(data)
    else:
        kfold = StratifiedKFold(n_splits=nfolds, shuffle=True, random_state=1)
        split_inds = kfold.split(data, labels)
        # kfold = KFold(n_splits=nfolds, shuffle=True, random_state=1)
        # split_inds = kfold.split(data)

    for fold, (train_index, test_index) in enumerate(split_inds):
        print(f'Beginning fold {fold}.')
        data_train, data_valid = data.iloc[train_index], data.iloc[test_index]
        if is_generator:
            train_data = data_class(data_train, generator)
            valid_data = data_class(data_valid, generator)
        else:
            train_data = data_class(data_train, labels[train_index])
            valid_data = data_class(data_valid, labels[test_index])

        if data_processing is not None:
            # WHen loading, the data here will be the same, so the processing information can be recalculated
            train_data.preprocess()
            # Scale the validation data using the quantities calculated on the training data
            valid_data.preprocess(train_data.get_preprocessing_info())

        model_instance = model(fold, device)

        # Train
        fold_directory = 'models'
        if load:
            load_model(model_instance, fold_directory, epochs)
        else:
            train_loss[fold], valid_loss[fold] = fit_model(model_instance, train_data, valid_data,
                                                           optimizer(model_instance), batch_size, epochs, device,
                                                           scheduler=scheduler, identifier=fold_directory)

        valid_data.data = valid_data.data.to(device)
        fold_info = evaluate_fold(model_instance, valid_data, sample_info=sample_info)

        y_true += [fold_info[0]]
        y_pred += [fold_info[1]]
        saliency += [fold_info[2]]
        hessian += [fold_info[3]]

    # Return the shapes of the different folds
    shapes = [len(y) for y in y_true]
    y_true = np.concatenate(y_true)
    y_pred = np.concatenate(y_pred)
    saliency = np.concatenate(saliency)
    hessian = np.stack([tensor2numpy(h) for h in hessian])

    svo = save_info(directory=outputdir, name=outputname)

    if load:
        # Loading is intended to run all validation steps again, so most of the values here shouldn't be redefined
        with open(svo.set_name('eval_info', '.np', sample_info=sample_info), 'rb') as file:
            _ = np.load(file)
            _ = np.load(file)
            _ = np.load(file)
            train_loss = np.load(file)
            valid_loss = np.load(file)
            _ = np.load(file)
    else:
        train_loss = np.mean(train_loss, 0)
        valid_loss = np.mean(valid_loss, 0)

    with open(svo.set_name('eval_info', '.np', sample_info=sample_info), 'wb') as file:
        np.save(file, y_true)
        np.save(file, y_pred)
        np.save(file, np.mean(saliency, 0))
        np.save(file, train_loss)
        np.save(file, valid_loss)
        np.save(file, shapes)

    return y_true, y_pred, saliency, hessian, train_loss, valid_loss, shapes
