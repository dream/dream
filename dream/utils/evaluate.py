import traceback
from dream.utils import hyperparameters

from .plotting import compute_and_plot_roc, compute_and_plot_salience, plot_features, plot_weights_dists, plot_roc, \
    hist_salience, plot_training, plot_samples, plot_hessian
import numpy as np

from sklearn.model_selection import KFold
import pandas as pd
from dream.utils.hyperparameters import get_data_class
from dream.utils.torch_utils import get_device, make_labels, tensor2numpy
from dream.utils.io import save_info, load_tensor, get_file_name

from dream.data.data_loaders import get_data, match_features

from torch import no_grad

from ..models.classifier import load_model


def evaluate_fold(model, test_data, sample_info=None, fold=0):
    model.eval()
    svo = save_info(model=model, start_sv=1)
    # Plot a roc plot for the trained classifier and calculate the AUC
    y_true, y_pred = compute_and_plot_roc(model, test_data, svo.set_name('ROC', '.png'), sample_info=sample_info)

    # Calculate the weights needed to corrrect the simulation such that it exactly matches Geant 4
    # plot_weights_dists(test_data, y_pred, svo, sample_info=sample_info)

    # Plot maps of the derivatives of the classifier with respect to each input feature
    saliency, hessian = compute_and_plot_salience(model, test_data, svo.set_name('saliency_{}', '.png'),
                                                  sample_info=sample_info)
    # Reverse the preprocessing and plot the features
    test_data.reverse_preprocessing()
    plot_features(test_data, svo.set_name('features', '.png'), sample_info=sample_info,
                  mean_derivatives=np.mean(saliency, 0))
    # Plot samples drawn from the model, if it can be sampled from, this is useful for evaluating anomaly detection
    # methods such as flows.
    if model.can_be_sampled():
        with no_grad():
            samples = model.sample(int(1e4)).cpu()
        test_data.data = test_data.data.cpu()
        plot_samples(test_data._invert_preprocessing(samples), test_data.data, svo.set_name('sampled_features', '.png'),
                     feature_names=test_data.feature_names)

    return y_true[:, 0], y_pred[:, 0], saliency, hessian


def evaluate_folds(y_true, y_pred, saliency, hessian, train_loss, valid_loss, directory, exp_name, all_data,
                   sample_info=None, anomaly_detection=False):
    svo = save_info(directory=directory, name=exp_name)
    plot_training(train_loss, valid_loss, svo.set_name('Training', '.png'))
    feature_names = all_data.feature_names
    auc = plot_roc(y_true, y_pred, svo.set_name('roc'), sample_info, add_legend=True)
    if anomaly_detection:
        # As the correlations are in distribution, we expect that an overdensity will show up as 'super' nominal cases
        # and so the AUC will be less than 0.5.
        auc = abs(auc - 0.5) + 0.5
    hist_salience(saliency, feature_names, svo.set_name('saliency_hists'), sample_info=None)
    with open(svo.set_name('auc', '.np', sample_info=sample_info), 'wb') as f:
        np.save(f, auc)
        np.save(f, np.mean(saliency, 0))
    model_name = sample_info
    plot_features(all_data, svo.set_name('features', '.png', sample_info=sample_info),
                  sample_info=model_name, mean_derivatives=np.mean(saliency, 0))
    plot_hessian(hessian.mean(0), all_data.feature_names, svo.set_name('hessian', '.png', sample_info=sample_info))


def apply_model(g4_data, fast_data, model, outputdir, outputname, nfolds, activation, nfeatures,
                data_processing, data_file='ad_test', anomaly_detection=True):
    """
    This function is meant for evaluation by default on anomaly detection files.
    """
    try:
        device = get_device()
        data_class = get_data_class(data_processing)

        y_true = []
        y_pred = []
        saliency = []
        hessian = []

        kfold = KFold(n_splits=nfolds, shuffle=True, random_state=1)
        split_inds = kfold.split(g4_data)

        for fold, (train_index, test_index) in enumerate(split_inds):
            # Need to append ones to take all of the AD data, or do something cleaner
            data = pd.concat((g4_data.iloc[test_index], fast_data), 0)
            labels = make_labels(g4_data.iloc[test_index], fast_data)
            ad_data = data_class(data, labels)

            model_instance = model(fold, device)
            fold_directory = 'models'
            load_model(model_instance, fold_directory)

            ad_data.data = ad_data.data.to(device)
            fold_info = evaluate_fold(model_instance, ad_data, sample_info=data_file)

            y_true += [fold_info[0]]
            y_pred += [fold_info[1]]
            saliency += [fold_info[2]]
            hessian += [fold_info[3]]

        y_true = np.concatenate(y_true)
        y_pred = np.concatenate(y_pred)
        saliency = np.concatenate(saliency)
        hessian = np.stack([tensor2numpy(h) for h in hessian])
        train_loss = [0]
        valid_loss = [0]
        evaluate_folds(y_true, y_pred, saliency, hessian, train_loss, valid_loss, outputdir, outputname, ad_data,
                       sample_info=data_file, anomaly_detection=anomaly_detection)
    except Exception as e:
        # print(e)
        # print(f'{data_file} is not compatible with the model.')
        print(traceback.format_exc())


def evaluate_on_files(args, data, ad_files, data_loader, model):
    print('Applying on anomalous data.')
    for ad_file in ad_files.split(','):
        ad_file_name, ad_info = get_file_name(args.data_dir, ad_file)
        ad_data = get_data(ad_file_name, include_integers=args.integers, data_loader=data_loader)
        g4_data, ad_data = match_features(data, ad_data)
        sample_info = ad_info
        apply_model(g4_data, ad_data, model, args.outputdir, args.outputname, args.nfolds,
                    hyperparameters.activations[args.activation], data.shape[1], args.data_processing,
                    data_file=sample_info)
