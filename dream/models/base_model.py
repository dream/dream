import os.path
import warnings

import torch
import torch.nn as nn
import numpy as np
from abc import ABC, abstractmethod

class BaseModel(ABC, nn.Module):

    def __init__(self, base_model, exp_name, directory, loss_name, activation):
        super(BaseModel, self).__init__()
        self.base_model = base_model
        self.exp_name = exp_name
        self.directory = directory
        self.loss_name = loss_name
        self.activation = activation

    @classmethod
    def get_scores(self, data, batch_size=None):
        return 0

    @classmethod
    def predict(self, data, batch_size=None):
        return 0

    def device(self):
        return next(self.parameters()).device

    @classmethod
    def compute_loss(self, data):
        """
        Compute the loss of the data, and set the self.loss attribute
        :param data: A set of inputs, targets and weights
        :return: the loss of the model to call backward() on
        """
        inputs, target, weights = data
        return 0

    def save(self, path, overwrite=True):
        if (not overwrite) and os.path.isfile(path):
            exit()
        else:
            torch.save(self.base_model.state_dict(), path)

    def load(self, path):
        self.base_model.load_state_dict(torch.load(path))

    def can_be_sampled(self):
        sample_op = getattr(self, "sample", None)
        if callable(sample_op):
            return True
