from nflows import transforms
from torch.nn import functional as F
from torch.nn import Identity


from dream.models.base_model import BaseModel


class Flow(BaseModel):

    def __init__(self, base_model, exp_name, loss_name='Likelihood', directory='BasicFlow'):
        super(Flow, self).__init__(base_model, exp_name, directory, loss_name, Identity())

    def forward(self, data):
        return self.base_model(data)

    def sample(self, num_samples):
        return self.base_model.sample(num_samples, None)

    def get_scores(self, data, batch_size=None):
        return self.base_model.log_prob(data)

    def predict(self, data, batch_size=None):
        # Return the minus here because the true class is zeros!
        return -self.get_scores(data, batch_size=batch_size).view(-1, 1)

    def compute_loss(self, data):
        inputs, targets, weights = data
        device = self.device()
        log_probs = self.base_model.log_prob(inputs[targets.view(-1) == 0].to(device))
        log_probs_unif = self.base_model.log_prob(inputs[targets.view(-1) == 1].to(device))
        # self.loss = - log_probs.mean() + log_probs_unif.mean() / 10
        self.loss = - log_probs.mean()
        return self.loss


def get_transform(inp_dim=1, nodes=64, num_blocks=2, nstack=2, tails='linear', tail_bound=1., num_bins=10,
                  context_features=None, lu=1, bnorm=0, spline=True, activation=F.leaky_relu):
    transform_list = []
    for i in range(nstack):

        if tails is not None:
            tb = tail_bound
        else:
            tb = tail_bound if i == 0 else None

        if spline:
            transform_list += [
                transforms.MaskedPiecewiseRationalQuadraticAutoregressiveTransform(inp_dim, nodes,
                                                                                   num_blocks=num_blocks,
                                                                                   tail_bound=tb, num_bins=num_bins,
                                                                                   tails=tails,
                                                                                   context_features=context_features,
                                                                                   activation=activation)]
        else:
            transform_list += [transforms.MaskedAffineAutoregressiveTransform(inp_dim, nodes, num_blocks=num_blocks,
                                                                              activation=activation,
                                                                              context_features=context_features)]

        if bnorm:
            transform_list += [transforms.BatchNorm(inp_dim)]

        if (tails is None) and (tail_bound is not None) and (i == nstack - 1):
            transform_list += [transforms.standard.PointwiseAffineTransform(-tail_bound, 2 * tail_bound)]

        if lu:
            transform_list += [transforms.LULinear(inp_dim)]
        else:
            transform_list += [transforms.ReversePermutation(inp_dim)]

    return transforms.CompositeTransform(transform_list[:-1])


def coupling_spline(inp_dim, maker, nstack=3, tail_bound=None, tails=None, lu=0,
                    num_bins=10, mask=[1, 0], unconditional_transform=True):
    transform_list = []
    for i in range(nstack):
        # If a tail function is passed apply the same tail bound to every layer, if not then only use the tail bound on
        # the final layer
        tpass = tails
        if tails:
            tb = tail_bound
        else:
            tb = tail_bound if i == 0 else None
        transform_list += [
            transforms.PiecewiseRationalQuadraticCouplingTransform(mask, maker, tail_bound=tb, num_bins=num_bins,
                                                                   tails=tpass,
                                                                   apply_unconditional_transform=unconditional_transform)]
        if (tails == None) and (not tail_bound == None) and (i == nstack - 1):
            transform_list += [transforms.standard.PointwiseAffineTransform(-tail_bound, 2 * tail_bound)]

        if lu:
            transform_list += [transforms.LULinear(inp_dim)]
        else:
            transform_list += [transforms.ReversePermutation(inp_dim)]

    return transforms.CompositeTransform(transform_list[:-1])


import nflows.nn as nn_
from nflows.utils import get_num_parameters, create_alternating_binary_mask

def get_transform_full(inp_dim=1, nodes=64, num_blocks=2, nstack=2, tails=None, tail_bound=1., num_bins=10,
                       context_features=1, lu=1, bnorm=0, model='rq_coupling', activation=F.leaky_relu,
                       dropout_probability=0.0):
    transform_list = []
    for i in range(nstack):

        if tails is not None:
            tb = tail_bound
        else:
            tb = tail_bound if i == 0 else None

        if model == 'affine-coupling':
            transform_list += [
                transforms.AffineCouplingTransform(mask=create_alternating_binary_mask(inp_dim, even=(i % 2 == 0)),
                                                   transform_net_create_fn=lambda in_features,
                                                                                  out_features: nn_.nets.ResidualNet(
                                                       in_features=in_features,
                                                       out_features=out_features,
                                                       hidden_features=nodes,
                                                       context_features=context_features,
                                                       num_blocks=num_blocks,
                                                       activation=F.relu,
                                                       dropout_probability=dropout_probability,
                                                       use_batch_norm=bnorm)
                                                   )
            ]
        else:
            transform_list += [transforms.MaskedAffineAutoregressiveTransform(inp_dim, nodes, num_blocks=num_blocks,
                                                                              activation=activation,
                                                                              context_features=context_features)]

        if bnorm:
            transform_list += [transforms.BatchNorm(inp_dim)]

        if (tails is None) and (tail_bound is not None) and (i == nstack - 1):
            transform_list += [transforms.standard.PointwiseAffineTransform(-tail_bound, 2 * tail_bound)]

        if lu:
            transform_list += [transforms.LULinear(inp_dim)]
        else:
            transform_list += [transforms.ReversePermutation(inp_dim)]

    return transforms.CompositeTransform(transform_list[:-1])
