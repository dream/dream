import glob

import torch.nn as nn
import torch.nn.functional as F

from dream.models.base_model import BaseModel
from dream.utils import hyperparameters

from dream.utils.io import save_info


class Classifier(BaseModel):

    def __init__(self, base_model, nfeatures, nclasses, exp_name, loss_object=F.binary_cross_entropy,
                 loss_name='Classification Error', directory='BasicClassifier', activation=nn.Identity()):
        super(Classifier, self).__init__(base_model(nfeatures, nclasses), exp_name, directory, loss_name, activation)
        self.loss_object = loss_object

    def forward(self, data):
        return self.activation(self.base_model(data))

    def get_scores(self, data, batch_size=None):
        return self.base_model.predict(data, batch_size=batch_size)

    def predict(self, data, batch_size=None):
        return self.activation(self.get_scores(data, batch_size=batch_size))

    def compute_loss(self, data):
        inputs, target, weights = data
        device = self.device()
        prediction = self.predict(inputs.to(device))
        self.loss = self.loss_object(prediction, target.to(device), weight=weights.to(device))
        return self.loss


def load_model(model, directory, n_epochs=None):
    svo = save_info(model, 0)
    model_dir = f'{svo.get_saving_dir(directory)}/saved_models/'
    if n_epochs is None:
        models = glob.glob(f'{model_dir}*')
        epoch = max([int(m.split('/')[-1]) for m in models])
    else:
        epoch = n_epochs - 1
    model_name = f'{model_dir}{epoch}'
    model.load(model_name)


class Model:

    def __init__(self, args, base_net, data_shape):
        self.args = args
        self.base_net = base_net
        self.data_shape = data_shape

    def __call__(self, fold, device):
        # Make a classifier object
        activation = hyperparameters.activations[self.args.activation]
        if isinstance(activation, str):
            classifier_activation = hyperparameters.activations[activation]
        else:
            classifier_activation = activation
        classifier = Classifier(self.base_net,
                                self.data_shape,
                                1,
                                f'{self.args.outputname}/fold{fold}',
                                directory=self.args.outputdir,
                                activation=classifier_activation
                                ).to(device)
        return classifier
