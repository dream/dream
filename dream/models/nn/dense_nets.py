import torch
import torch.nn as nn
from copy import copy

from dream.utils.torch_utils import DataOnly, to_tensor
from torch.utils.data import DataLoader


def norm_exception():
    raise Exception("Can't set both layer and batch normalization")


class MLP(nn.Module):
    def __init__(self, input_dim, output_dim, islast=True, output_activ=nn.Identity(), layers=[128, 128, 64], drp=0,
                 batch_norm=False, layer_norm=False, int_activ=torch.relu, change_init=False):
        super(MLP, self).__init__()
        layers = copy(layers)

        self.latent_dim = output_dim
        self.drp_p = drp
        self.inner_activ = int_activ

        self.functions = nn.ModuleList([nn.Linear(input_dim, layers[0])])
        if islast:
            layers += [output_dim]
        self.functions.extend(nn.ModuleList([nn.Linear(layers[i], layers[i + 1]) for i in range(len(layers) - 1)]))

        self.output_activ = output_activ

        if batch_norm and layer_norm:
            norm_exception()

        self.norm = 0
        # self.norm_func = nn.LayerNorm
        if batch_norm:
            self.norm = 1
            self.norm_func = nn.BatchNorm1d
        if layer_norm:
            self.norm = 1
            self.norm_func = nn.LayerNorm
        if self.norm:
            self.norm_funcs = nn.ModuleList([self.norm_func(layers[i]) for i in range(len(layers) - 1)])

    def forward(self, x):
        for i, function in enumerate(self.functions[:-1]):
            x = function(x)
            if self.norm:
                x = self.norm_funcs[i](x)
            x = self.inner_activ(x)
            x = nn.Dropout(p=self.drp_p)(x)
        x = self.output_activ(self.functions[-1](x))
        return x

    def _forward_on_batched(self, data):
        store = []
        for data in data:
            store += [self(data)]
        return torch.cat(store)

    def predict(self, data, batch_size=None):
        """
        This method wraps the forward method into a function that can be applied to batched data, as well as
        batching the data and applying the forward method.
        """
        if isinstance(data, list):
            raise Exception('Lists must be converted to numpy arrays or tensors before calling predict.')
        if (len(data.shape) == 2) and (batch_size is None):
            return self(to_tensor(data, dtype=torch.float32))
        elif (len(data.shape) == 2) and (batch_size is not None):
            data = DataLoader(DataOnly(data), batch_size=batch_size)
        return self._forward_on_batched(data)
