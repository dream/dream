import pdb
import subprocess, os
from collections import defaultdict

from dream.utils.io import save_info, get_properties, DATA_SUBFOLDER_NAME
import glob
import numpy as np


def add_text(file, to_add, is_end=False):
    if not is_end:
        to_add += '\nFILLER'
    return file.replace('FILLER', to_add)


def generate_report_on_slice(directory, auc, features_images, pdf, report_name='overall_report', last=True,
                             hessians=None):
    # Reverse sort the arrays by auc
    sorted_inds = np.argsort(-auc)
    auc = auc[sorted_inds]
    features_images = np.array(features_images)[sorted_inds]
    hessians_passed = hessians is not None
    width = 60 if hessians_passed else 100

    html_str = '<!-- START PLOTS --> \n'
    for i, (auc, image) in enumerate(zip(auc, features_images)):
        name = image.split('/')[-1]
        # With the class attribute you can write some javascript to toggle the different reports on each slice
        html_str += '<figure class="left"> \n'
        image = image.split(DATA_SUBFOLDER_NAME)[-1].split('/', 2)[-1]
        html_str += f'<img src="{image}" id="{name}" class="plot {report_name}" alt="{name}" style="width:{width}%"> \n'
        html_str += '</figure> \n'
        if hessians_passed:
            html_str += '<figure class="right"> \n'
            html_str += f'<img src="{hessians[i]}" id="{name}" class="plot {report_name}" alt="{name} hessian" style="width:20%"> \n'
            html_str += '</figure> \n'
        html_str += f'<h1>AUC: {auc:.4f}</h1> \n'

    with open('dream/reports/templates/report_on_slice.html', 'r') as html_template:
        html_template = html_template.read()
        html_str = add_text(html_template, html_str, is_end=True)

    html_report_path = f'{directory}/'
    os.makedirs(html_report_path, exist_ok=True)
    tex_file = f'{report_name}.html'
    with open(html_report_path + tex_file, 'w') as report:
        report.write(html_str)


def make_html_table(auc, unique_items, items, models):
    str = '<TABLE class="styled-table">\n<CAPTION>Ordered by best AUC </CAPTION> \n'
    str += '<thead>\n<tr>\n<th> </th>\n<th>AUC</th>\n<th></th>\n</tr>\n</thead>\n<tbody>'
    # First order the unique_items by the average auc
    mn = []
    unique_items = np.array(list(unique_items))
    for unique_slice in unique_items:
        mx = make_mask(unique_slice, items)
        mn += [np.mean(auc[mx])]
    idx = np.argsort(np.array(mn))

    for unique_slice in unique_items[idx]:
        mx = make_mask(unique_slice, items)
        inds = np.argsort(auc[mx])
        c_models = mask_list(models, mx)

        def get_entry(index, bold=False):
            style = ' style="font-weight:bold"' if bold else ''
            return f'<TD{style}> {auc[mx][index]: .3f} </TD><TD> {c_models[index]} </TD>'

        str += f'<TR class="active-row">\n <TD ROWSPAN="{len(c_models)}"> {unique_slice} </TD> {get_entry(inds[0], bold=True)} \n</TR>\n'
        for ind in inds[1:]:
            str += f'<TR>\n {get_entry(ind)} \n</TR>\n'
    str += '</tbody>\n</TABLE>\n<br>\n<br>\n'
    return str


def generate_global_report(directory, auc, file_names, report_name='overall_report', slices=True):
    if slices:
        slices = []
        models = []
        for file in file_names:
            slices += ['{}_{}_{}'.format(*file.split('_')[:3])]
            models += [file.split('_')[3].split('.root')[0]]
    else:
        slices = file_names
        models = file_names
    unique_slices = set(slices)
    unique_models = set(models)

    with open('dream/reports/templates/global.html', 'r') as html_template:
        text = html_template.read()
        html_report = text.replace('TITLE', 'Overall Report')
        table_1 = make_html_table(auc, unique_slices, slices, models)
        html_report = add_text(html_report, table_1)
        table_2 = make_html_table(auc, unique_models, models, slices)
        html_report = add_text(html_report, table_2, is_end=True)

        os.makedirs(directory, exist_ok=True)
        tex_file = f'{report_name}.html'
        with open(os.path.join(directory, tex_file), 'w') as report:
            report.write(html_report)


def make_mask(value, lst):
    return [elem == value for elem in lst]


def mask_list(lst, mask):
    return [elem for elem, mx in zip(lst, mask) if mx]


def get_experiments(directory):
    """
    Find all the json files under a given directory.
    :param directory: The directory to search in, across all levels
    :return: list of directories
    """
    if not os.path.isdir(directory):
        raise Exception(f'No directory {directory}.')
    return glob.glob(os.path.join(directory, '**/*.json'), recursive=True)


def get_info(all_experiments, slices, get_properties=get_properties, add_hessians=False):
    info = defaultdict(list)
    for i, experiment in enumerate(all_experiments):
        try:
            svo = save_info().init_from_json(experiment)
            args = svo.read_experiment()
            for data_file in args['data_files'].split(',')[1:]:
                with open(svo.set_name('auc', '.np', sample_info=data_file), 'rb') as f:
                    info['auc'] += [np.load(f)]
                info['features_images'] += [svo.set_name('features', '.png', sample_info=data_file)]
                if add_hessians:
                    info['hessians'] += [svo.set_name('hessian', '.png', sample_info=data_file)]
                if slices:
                    particle, et, en, _ = get_properties(data_file)
                else:
                    particle, en, et = 0, 0, 0
                info['particle_type'] += [particle]
                info['energy'] += [en]
                info['eta'] += [et]
                info['sim_file'] += [data_file]
        except Exception as e:
            print(e)
            print(f'Run on {experiment} failed.')
    return info


def report_on_slices(directory, pdf, info, add_hessians=False):
    """
    Generate a report on each of the slices stored under directory.
    :param directory: The location for saving the reports on each slice.
    :param pdf: Whether or not to generate a pdf for this report.
    :param info: A dictionary of lists, contains the information needed to generate this report. Output of get_info.
    :return:
    """
    auc = np.array(info['auc'])
    particle_types = set(info['particle_type'])
    etas = set(info['eta'])
    energies = set(info['energy'])
    if add_hessians:
        hessians = info['hessians']
    else:
        hessians = None
    for p in particle_types:
        for et in etas:
            for en in energies:
                p_mask = make_mask(p, info['particle_type'])
                et_mask = make_mask(et, info['eta'])
                en_mask = make_mask(en, info['energy'])
                mask = [a and b and c for a, b, c in zip(p_mask, et_mask, en_mask)]
                if sum(mask) > 0:
                    generate_report_on_slice(directory, auc[mask], mask_list(info['features_images'], mask), pdf,
                                             report_name=f'{p}_{en}_{et}', hessians=hessians)


def generate_meta_report(directory, pdf=True, slices=True, get_properties=get_properties, add_hessians=False):
    """
    A function for sumarising multiple runs.
    :param directory: The name of the directory where the files are stored.
    """

    all_experiments = get_experiments(directory)
    info = get_info(all_experiments, slices, get_properties, add_hessians=add_hessians)

    auc = np.array(info['auc'])

    # Generate a report on each of the slices contained under directory
    report_on_slices(directory, pdf, info, add_hessians=add_hessians)
    # Generate a global summary across all of the files under directory
    generate_global_report(directory, auc, info['sim_file'], report_name='overall_report',
                           slices=slices)


def main():
    generate_meta_report('/Users/samklein/PycharmProjects/DREAM/dream/images/photons')


if __name__ == '__main__':
    main()
