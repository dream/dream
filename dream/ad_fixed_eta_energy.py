import yaml
from dream.data.data_loaders import get_data, match_features
from dream.data.physics_datasets import is_integers
from dream.models.classifier import Classifier, Model
from dream.models.nn.dense_nets import MLP
from dream.utils.evaluate import evaluate_fold, evaluate_folds, apply_model, evaluate_on_files
from dream.utils.hyperparameters import get_data_class
from dream.utils.sampling import AnomalyGenerator
from dream.utils.training import train_model_nfolds
from dream.utils import hyperparameters
from dream.utils.io import save_info, get_file_name, load_tensor, update_attribtue

import argparse


def get_anomaly_args():
    parser = argparse.ArgumentParser()

    # Additional configuration
    parser.add_argument('--yaml_configs', type=str, default=None,
                        help='Yaml file that can be used to set some keyword arguments.')

    # Saving
    parser.add_argument('-d', '--outputdir', type=str, default='ad_1',
                        help='Choose the base output directory')
    parser.add_argument('-n', '--outputname', type=str, default='test',
                        help='Set the output name directory')
    parser.add_argument('--load', type=int, default=0,
                        help='Load and existing model?')

    # Training
    parser.add_argument('--batch_size', type=int, default=1000, help='Size of batch for training.')
    parser.add_argument('--epochs', type=int, default=100,
                        help='The number of epochs to train for.')
    parser.add_argument('--nfolds', type=int, default=5,
                        help='The number of folds to train the classifier on.')
    parser.add_argument('--optim', type=str, default='Adam',
                        help='The optimizer to use for training.')
    parser.add_argument('--scheduler', type=str, default='cosine',
                        choices=['none', 'cosine', 'plateau'],
                        help='The optimizer to use for training.')
    parser.add_argument('--lr', type=float, default=0.0001,
                        help='The learning rate.')
    parser.add_argument('--wd', type=float, default=0.001,
                        help='The weight decay parameter to use in the AdamW optimizer.')
    parser.add_argument('--p', type=float, default=0.9,
                        help='The momentum to use in SGD.')

    # Model
    parser.add_argument('--activation', type=str, default='sigmoid',
                        help='The activation function to apply to the output of the classifier.')
    parser.add_argument('--width', type=int, default=32,
                        help='The activation function to apply to the output of the classifier.')
    parser.add_argument('--depth', type=int, default=3,
                        help='The activation function to apply to the output of the classifier.')
    parser.add_argument('--batch_norm', type=int, default=0,
                        help='Use batch normalisation?')
    parser.add_argument('--layer_norm', type=int, default=0,
                        help='Use layer normalisation?')
    parser.add_argument('--dropout', type=float, default=0.0, help='Dropout percentage?')

    # Data
    parser.add_argument('--data_files', type=str,
                        default='pions_262144_20_G4.root,pions_262144_20_FCSGAN.root,pions_262144_20_FCS.root',
                        help='The name of the files to load.')
    parser.add_argument('--evaluate_files', type=str, default='pions_262144_20_FCS.root',
                        help='The name of the files to load.')
    parser.add_argument('--data_dir', type=str, default='dream/data/downloads/',
                        help='The data directory to use to access the samples, set to None if they are not in the same '
                             'place.')
    parser.add_argument('--data_processing', type=str, default='normalise',
                        choices=['normalise', 'standardise', 'log_scaled'],
                        help='The type of data processing to apply')
    parser.add_argument('--integers', type=int, default=0, choices=[0, 1], help='Include integer features?')

    parser.add_argument('--data_loader_file', type=str, default=None,
                        help='A file from which a data loader can be imported.')

    args = parser.parse_args()

    if args.yaml_configs is not None:
        # Update the args using a configuration file
        with open(args.yaml_configs, 'r') as stream:
            config_dict = yaml.safe_load(stream)
        for key, value in config_dict.items():
            update_attribtue(args, key, value)

    return args


def simple_anomaly_detection(args, data_loader):
    svo = save_info(directory=args.outputdir, name=args.outputname)
    svo.register_experiment(args)

    # If an args data_loader_file is passed load it!
    if args.data_loader_file is not None:
        funcs = __import__(args.data_loader_file, globals(), locals(), ['data_loader'])
        data_loader = funcs.data_loader

    # Define the model that will be used for classification
    def base_net(input_dim, output_dim):
        return MLP(input_dim, output_dim, layers=[int(args.width / (1 - args.dropout))] * args.depth, drp=args.dropout,
                   batch_norm=args.batch_norm, layer_norm=args.layer_norm)

    # Make an object that will return an optimizer object
    optimizer = hyperparameters.optimizer(args.optim, args.lr, args.p, args.wd)

    # Load the data
    print('Loading Data')
    train_file, ad_files = args.data_files.split(',', 1)
    g4_file_name, g4_info = get_file_name(args.data_dir, train_file)
    data = get_data(g4_file_name, include_integers=args.integers, data_loader=data_loader)
    sample_info = f'ad_{g4_info}'
    num_samples = data.shape[0]
    print(f'There are {num_samples} Geant4 samples to train on.')

    # Split the data
    data = data.sample(frac=1)
    n_mix = int(0.1 * num_samples * (1 - 1 / args.nfolds))
    data_to_mix = data.iloc[:n_mix]
    data = data.iloc[n_mix:]

    anomaly_generator = AnomalyGenerator(data_to_mix)

    model = Model(args, base_net, data.shape[1])

    if not args.load:
        # Otherwise the classifier is loaded in apply classifier.
        y_true, y_pred, saliency, hessian, train_loss, valid_loss, shapes = train_model_nfolds(data, anomaly_generator,
                                                                                               model, optimizer,
                                                                                               args.epochs,
                                                                                               args.batch_size,
                                                                                               nfolds=args.nfolds,
                                                                                               data_processing=args.data_processing,
                                                                                               scheduler=args.scheduler,
                                                                                               sample_info=sample_info,
                                                                                               outputdir=args.outputdir,
                                                                                               outputname=args.outputname)
        # Evaluate the models
        data_class = get_data_class(args.data_processing)
        all_data = data_class(data, anomaly_generator)
        all_data.update_with_generation()
        evaluate_folds(y_true, y_pred, saliency, hessian, train_loss, valid_loss, args.outputdir, args.outputname,
                       all_data, sample_info=sample_info)

    # Load some FCS data and run a test
    evaluate_on_files(args, data, ad_files, data_loader, model)


if __name__ == '__main__':
    args = get_anomaly_args()
    data_loader = None
    simple_anomaly_detection(args, data_loader)
