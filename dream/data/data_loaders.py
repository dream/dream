import uproot
import pandas as pd

from .physics_datasets import is_integers


def load_ntuple(file_name, features=None, include_integers=True):
    with uproot.open(file_name) as tree:
        key = tree.keys()
        if len(key) != 1:
            raise Exception(
                'The current data loading method only works if the tree from the root file only has one key.')
        events = tree[key[0]]
        df = events.arrays(events.keys(), library='pd')

    if features is not None:
        if not isinstance(features, list):
            features = features.split(',')
        df = df[features]

    if not include_integers:
        df = df.iloc[:, ~is_integers(df)]

    return df, file_name.split('/')[-1].split('.root')[0]


def match_features(g4_features, p_features):
    indx = g4_features.keys().intersection(p_features.keys())
    return g4_features[indx], p_features[indx]


def get_data(g4_file, sim_file=None, features=None, include_integers=False, data_loader=None):
    if data_loader is None:
        data_loader = load_ntuple
    g4_features, g4_info = data_loader(g4_file, features=features, include_integers=include_integers)
    if not isinstance(g4_features, pd.DataFrame):
        raise Exception('Data loader must return a pandas dataframe')
    if sim_file is not None:
        p_features, sim_info = data_loader(sim_file, features=features, include_integers=include_integers)
        return *match_features(g4_features, p_features), sim_info
    else:
        return g4_features
