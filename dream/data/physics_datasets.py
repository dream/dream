# Make a class to wrap the dataset and use with torch's dataloader

from torch.utils.data import Dataset
import torch
from abc import ABC, abstractmethod
import numpy as np
import pandas as pd


def is_integers(df):
    if isinstance(df, pd.DataFrame):
        return np.array([all(df[key] % 1 == 0) for key in df.keys()])
    elif isinstance(df, np.ndarray):
        return [all(col % 1 == 0) for col in df.transpose()]
    elif isinstance(df, torch.Tensor):
        return [all(col % 1 == 0) for col in df.t()]
    else:
        raise Exception(f'Type not supporeted for {df}')


class DataClass(ABC, Dataset):
    def __init__(self, dataframe, labels_or_generator, sample_weight=None, dtype='float32'):
        super(DataClass, self).__init__()

        self.is_generator = callable(labels_or_generator)
        self.ndata, self.nfeatures = dataframe.shape
        dataframe = self.process_dataframe(dataframe)
        self.feature_names = list(dataframe.keys())

        # Strip out NaNs and check the dataframe isn't empty
        self.check_for_nans(dataframe)
        self.check_isempty(dataframe)
        dataframe, labels_or_generator = self.dropna(dataframe, labels_or_generator)
        self.check_isempty(dataframe)

        self.data = torch.tensor(dataframe.to_numpy(dtype))
        self.dtype = dtype

        if self.is_generator:
            self.base_data = self.data
            self.generator = labels_or_generator
            labels = np.concatenate(
                (np.zeros(self.ndata, dtype=np.float32), np.ones(self.ndata, dtype=np.float32)))
        else:
            self.generator = None
            labels = labels_or_generator
            assert dataframe.shape[0] == labels.shape[0]
        self.targets = torch.tensor(labels).view(-1, 1)

        # No preprocessing applied at initialization
        self.preprocessed = False
        self.preprocessing_info_available = False

        if sample_weight is None:
            n_ones = sum(self.targets)
            n_zeros = len(self.targets) - n_ones
            # Assign weights, note that they shouldn't be greater than one, if num classes are equal then
            # self.sample_weight=1 for every sample
            self.sample_weight = torch.ones_like(self.targets)
            if n_ones != n_zeros:
                if n_ones > n_zeros:
                    self.sample_weight[self.targets == 0] = n_ones / n_zeros
                if n_ones < n_zeros:
                    self.sample_weight[self.targets == 1] = n_zeros / n_ones

        else:
            self.sample_weight = sample_weight

    def process_dataframe(self, df):
        self.integer_mask = is_integers(df)
        return df

    def check_for_nans(self, data=None):
        if data is not None:
            has_nans = pd.isnull(data).to_numpy().sum()
        else:
            has_nans = torch.isnan(self.data).sum()
        if has_nans > 0:
            print(f'Data has {has_nans} NaN values.')
        return has_nans

    def check_isempty(self, dataframe):
        if dataframe.empty:
            raise Exception('Dataframe is empty.')

    def dropna(self, data=None, labels=None):
        """Remove NaNs from both data and corresponding labels, either for the attributes or passed data."""
        if data is not None:
            mx = ~pd.isnull(data).any(1)
            data = data.loc[mx]
            if (labels is not None) and (not self.is_generator):
                labels = labels[mx]

            return data, labels
        else:
            mx = ~torch.isnan(self.data).any(1)
            self.data = self.data[mx]
            self.targets = self.targets[mx]


    def reorder_columns(self, list):
        if list is not None:
            inds = sorted(range(len(list)), key=lambda j: list[j], reverse=True)
        else:
            inds = np.arange(self.nfeatures)
        return self.data[:, inds], self.targets, [self.feature_names[i] for i in inds]

    @abstractmethod
    def _calculate_preprocessing_info(self):
        raise NotImplementedError

    def get_preprocessing_info(self, get_device_of=None):
        """
        :param get_device_of: data to which the preprocessing will be applied.
        :return: information that can be used to preprocess self.data or get_device_of. This info is calculated using
                 self.data
        """
        if self.preprocessing_info_available:
            info = self.preprocessing_info
        else:
            info = self._calculate_preprocessing_info()
            self.set_preprocessing_info(info)

        if get_device_of is not None:
            # If data is passed then put the information on the same device
            device = self.data.device
            try:
                for i in range(len(info)):
                    info[i] = info[i].to(device)
            except:
                info = info.to(device)

        return info

    def set_preprocessing_info(self, info=None):
        if self.preprocessed:
            raise NotImplementedError('This dataset has already been preprocessed, and changing the preprocssing info'
                                      'after this has been done is not supported.')
        elif info is None:
            self.preprocessing_info = self.get_preprocessing_info()
        else:
            self.preprocessing_info = info

        self.preprocessing_info_available = True

    @abstractmethod
    def _apply_preprocessing(self):
        raise NotImplementedError

    def preprocess(self, info=None):
        self.set_preprocessing_info(info)
        if not self.preprocessed:
            self._apply_preprocessing()
        self.preprocessed = True
        if self.is_generator:
            self.update_with_generation()

    def update_with_generation(self):
        if self.is_generator:
            sample = torch.tensor(
                self.generator(self.ndata, min_val=self.base_data.min(0)[0].numpy(),
                               max_val=self.base_data.max(0)[0].numpy()),
                dtype=self.base_data.dtype
            )
            self.data = torch.cat((self.base_data, sample), 0)
            if self.preprocessed:
                self._apply_preprocessing()

    @abstractmethod
    def _invert_preprocessing(self):
        raise NotImplementedError

    def reverse_preprocessing(self):
        if self.preprocessed:
            self._invert_preprocessing()
        self.preprocessed = False

    def to(self, device):
        self.data = self.data.to(device)
        self.targets = self.targets.to(device)
        self.sample_weight = self.sample_weight.to(device)

    def __getitem__(self, item):
        return self.data[item], self.targets[item], self.sample_weight[item]

    def __len__(self):
        return self.data.shape[0]


class NormalisedData(DataClass):

    def __init__(self, *args, quantile=0.01, **kwargs):
        super(NormalisedData, self).__init__(*args, **kwargs)
        self.quantile = quantile

    def _calculate_preprocessing_info(self):
        min_vals = self.data.quantile(self.quantile, 0)
        max_vals = self.data.quantile(1 - self.quantile, 0)
        return [min_vals, max_vals]

    def _apply_preprocessing(self):
        min_vals, max_vals = self.get_preprocessing_info(self.data)
        self.data = (self.data - min_vals) / (max_vals - min_vals + 1e-8)

    def _invert_preprocessing(self, data=None):
        data_bool = data is None
        if data_bool:
            data = self.data
        min_vals, max_vals = self.get_preprocessing_info(data)
        data = data * (max_vals - min_vals + 1e-8) + min_vals
        if data_bool:
            self.data = data
        else:
            return data


class LogScaledData(DataClass):

    def _calculate_preprocessing_info(self):
        min_vals = self.data.min(dim=0).values
        max_vals = self.data.max(dim=0).values
        return [min_vals, max_vals]

    def _apply_preprocessing(self):
        min_vals, max_vals = self.get_preprocessing_info(self.data)
        self.data = (self.data - min_vals) / (max_vals - min_vals + 1e-8)
        self.data = torch.log(self.data + 1)

    def _invert_preprocessing(self):
        min_vals, max_vals = self.get_preprocessing_info(self.data)
        self.data = torch.exp(self.data) - 1
        self.data = self.data * (max_vals - min_vals + 1e-8) + min_vals


class StandardisedData(DataClass):

    def _calculate_preprocessing_info(self):
        return list(torch.std_mean(self.data, dim=0))

    def _apply_preprocessing(self):
        stds, means = self.get_preprocessing_info(self.data)
        self.data = (self.data - means) / (stds + 1e-8)

    def _invert_preprocessing(self):
        stds, means = self.get_preprocessing_info(self.data)
        self.data = self.data * (stds + 1e-8) + means
